package rib

import (
	"errors"
	"fmt"
	"os"
	"reflect"

	"gitlab.com/mae.earth/rigo/ri"
)

/* TODO:
 * the ri prototypes, as machine generated from the header files is incorrect,
 * as the RIB version of the commands are generally not the exact same.
 *
 * Solution, hand annotate the functions for RIB entry, testing against the output
 * from prman. Then adjust the machine generation to take into account these annotations.
 */

var (
	ErrInvalidContextHandle = errors.New("Invalid Context Handle")
)

type Runner2 struct {
	ctx *ri.RtContextHandle

	fun      *token_composite
	stack    []*token_composite
	callback ri.RtArchiveCallback
}

func (r *Runner2) ErrorHandler(code, severity int, msg string) {

	fmt.Fprintf(os.Stderr, "[error] %03d:%05d %s\n", severity, code, msg)
}

func NewRunner2(ctx *ri.RtContextHandle, useeh bool) (*Runner2, error) {

	if ctx == nil {
		return nil, ErrInvalidContextHandle
	}

	r := &Runner2{}
	/*if useeh {
		ctx.ErrorHandler(r.ErrorHandler)
	}*/
	r.ctx = ctx
	r.stack = make([]*token_composite, 0)

	return r, nil
}

func continue_runner2(runner *Runner2, token_stream []*token_composite) error {

	return nil
}

/*
type Runner struct {
	ctx *ri.RtContextHandle

	fun      *token_composite   
	stack    []*token_composite 
	callback ri.ArchiveCallback
}

func (r *Runner) Errorhandler(code, severity int, msg string) {

	fmt.Fprintf(os.Stderr, "[error] %03d:%05d %s\n", severity, code, msg)
}

func NewRunner(ctx *ri.RtContextHandle, useeh bool) (*Runner, error) {

	if ctx == nil {
		return nil, ErrInvalidContextHandle
	}

	r := &Runner{}
	if useeh {
		ctx.ErrorHandler(r.Errorhandler)
	}
	r.ctx = ctx

	r.stack = make([]*token_composite, 0)

	return r, nil
}

func continue_runner(runner *Runner, token_stream []*token_composite) error {

	

	for _, tc := range token_stream {

		//fmt.Printf("%05d\t%s\n", i+1, tc)

		switch tc.RiType {
		case "func", "EOF":
			if tc.Value == nil {

			} else {
				if runner.fun != nil {

					args := make([]reflect.Value, 0)

					for _, val := range runner.stack {
						if val.Value != nil {
							switch reflect.TypeOf(val.Value).String() {
							case "float64":

								args = append(args, reflect.ValueOf(val.Value.(float64)))
								break
							case "string":

								args = append(args, reflect.ValueOf(val.Value.(string)))
								break
							case "[]string":

								args = append(args, reflect.ValueOf(val.Value.([]string)))
								break
							case "int":

								args = append(args, reflect.ValueOf(val.Value.(int)))
								break
							case "[]int":

								args = append(args, reflect.ValueOf(val.Value.([]int)))
								break
							case "[]float64":

								args = append(args, reflect.ValueOf(val.Value.([]float64)))
								break
							case "[6]float64":

								args = append(args, reflect.ValueOf(val.Value.([6]float64)))
								break
							case "bool":

								args = append(args, reflect.ValueOf(val.Value.(bool)))
								break
							}
						} else {
							//args = append(args,nil)
						}
					}
					ctx := reflect.ValueOf(runner.ctx)


					if runner.fun.RiName != "End" && runner.fun.RiName != "Begin" {
						call := ctx.MethodByName(runner.fun.RiName)
						if call.IsValid() {

							fmt.Printf("+-------> calling into %s\n", runner.fun.RiName)


							switch runner.fun.RiName {
							case "ReadArchive":

								var f ri.ArchiveCallback 

								if len(args) == 1 {
									args = append(args, reflect.ValueOf(f))
								} else {

									args[1] = reflect.ValueOf(f)
								}
								break
							case "ArchiveRecord":

								if runner.callback != nil {

									runner.callback("structure", "this is %v", true) 
								}
								break
							}

							//fmt.Printf("+-----------> calling into %s with %d args\n\n", runner.fun.RiName, len(args))
							call.Call(args)
						}
					}
				}
				runner.fun = tc
				runner.stack = make([]*token_composite, 0)
			}
			break
		default:
			if tc.RiType != "" && tc.RiName != "" {
				runner.stack = append(runner.stack, tc)
			}
			break
		}
	}

	return nil
}
*/
