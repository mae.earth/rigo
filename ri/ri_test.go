package ri

import (
	"os"
	"strings"
	"sync"

	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

/* test driver, creates a buffer to check with */
type test_driver struct {
	mux sync.RWMutex
	rib string
}

func (d *test_driver) Write(p []byte) (int, error) {
	d.mux.Lock()
	defer d.mux.Unlock()

	d.rib = string(p)

	return len(p), nil
}

func (d *test_driver) Read() string {
	d.mux.RLock()
	defer d.mux.RUnlock()

	out := strings.TrimSpace(d.rib)
	if out[len(out)-1] == '\n' {
		return out[:len(out)-1]
	}
	return out
}

func Test_Ri(t *testing.T) {

	Convey("Ri", t, func() {

		f, err := os.Create("test.rib")
		if err != nil {
			panic(err)
		}
		defer f.Close()

		ctx := NewRiWrapper()

		ctx.Begin(f)

		ctx.ArchiveRecord(STRUCTURE, "%s RIB-Structure %.1f", RENDERMAN, 1.1)

		ctx.Display("example.exr", "openexr", "rgba")
		ctx.Format(640, 480, 1)
		ctx.Projection("perspective", "float fov", 30.3345)
		ctx.Hider("raytrace", "int maxsamples", 32, "int minsamples", 16, "int incremental", 100)
		ctx.Integrator("PxrPathTracer", "production")

		ctx.WorldBegin()
		ctx.Translate(0, 0, 10)

		ctx.AttributeBegin()
		ctx.Bound([6]float64{-1, -1, -1, 1, 1, 1})
		ctx.Translate(5, 5, 5)
		ctx.Light("PxrEnvDayLight", "light1")
		ctx.Geometry("envsphere", "constant float[2] resolution", [2]float64{1024, 1024})
		ctx.AttributeEnd()

		ctx.AttributeBegin()
		ctx.Translate(1, 2, 3)
		ctx.Bxdf("PxrDisney", "surface", "color baseColor", []float64{1, 0, 0})
		ctx.Sphere(1, -1, 1, 360)
		ctx.AttributeEnd()

		ctx.WorldEnd()

		ctx.End()

	})
}
