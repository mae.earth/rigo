module mae.earth/rigo

go 1.14

require (
	github.com/reusee/mmh3 v0.0.0-20140820141314-64b85163255b
	github.com/smartystreets/goconvey v1.6.4
	gitlab.com/mae.earth/rigo v0.0.0-20171030135659-e767898fde52
)
