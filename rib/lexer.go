package rib

import (
	"fmt"
	"mae.earth/rigo/ri"
)

/* TODO:
 * the ri prototypes, as machine generated from the header files is incorrect,
 * as the RIB version of the commands are generally not the exact same.
 *
 * Solution, hand annotate the functions for RIB entry, testing against the output
 * from prman. Then adjust the machine generation to take into account these annotations.
 */

type Lexer struct {
	info       *ri.Information
	count      int
	prototypes map[string]*ri.Information
}

func NewLexer() *Lexer {

	l := &Lexer{}
	l.prototypes = ri.RiPrototypes()
	/* we need to overide 'version' here to meet our requirements */
	l.prototypes["version"] = &ri.Information{Name: "version", Arguments: []ri.Argument{{Type: "float", Name: "ver"}}, Parameterlist: false}
	l.prototypes["#struct"] = &ri.Information{Name: "struct", Arguments: []ri.Argument{{Type: "string...", Name: "-"}}, Parameterlist: false} /* string... means 0 or more strings */
	l.prototypes["#comment"] = &ri.Information{Name: "comment", Arguments: []ri.Argument{{Type: "string...", Name: "-"}}, Parameterlist: false}

	return l
}

/* continue_lexer */
func continue_lexer(lexer *Lexer, token_stream []*token_composite) ([]*token_composite, error) {

	out_token_stream := make([]*token_composite, 0)

	for _, tc := range token_stream {

		if len(tc.Tokens) == 0 {
			break
		}

		switch tc.Tokens[0].Type {
		case TokenCommand:
			lexer.info = lexer.prototypes[tc.Tokens[0].Word]
			if lexer.info == nil {
				fmt.Printf(">>> unable to find \"%s\"\n", tc.Tokens[0].Word)
				return nil, nil /* FIXME error */
			}
			lexer.count = -1
			//fmt.Printf("using info %+v\n", lexer.info)
			tc.RiType = "func"
			tc.RiName = tc.Tokens[0].Word
			break
		case TokenArchive:
			tc.RiType = "archive"

			if tc.Tokens[0].RiType != "" {
				tc.RiName = tc.Tokens[0].RiType
				lexer.info = lexer.prototypes["#"+tc.Tokens[0].RiType]

			} else {
				tc.RiName = "comment"
				lexer.info = lexer.prototypes["#comment"]
			}
			lexer.count = -1
			if lexer.info == nil {
				fmt.Printf(">>> unable to find \"#%s\"\n", tc.Tokens[0].RiType)
				return nil, nil /* FIXME error */
			}
			break
		case TokenWord, TokenLastWord: /* generally a number or comment, otherwise in error */
			if lexer.info == nil {
				continue /* we can skip */
			}
			if lexer.info.Name == "struct" || lexer.info.Name == "comment" {
				tc.RiType = "string"
				tc.RiName = lexer.info.Name
			} else {
				if tc.Tokens[0].RiType == "number" {

					if lexer.count < len(lexer.info.Arguments) {
						/* fixme, check against info */
						tc.RiType = lexer.info.Arguments[lexer.count].Type
						tc.RiName = lexer.info.Arguments[lexer.count].Name
					} else {
						if !lexer.info.Parameterlist {
							tc.RiError = "invalid"
						}
						tc.RiType = "float" /* this is the general case, might be an int */
						tc.RiName = fmt.Sprintf("_plist(%d)_", lexer.count)
					}
				}
			}
			break
		case TokenOpen, TokenClose:
			if lexer.info == nil {
				continue
			}

			if lexer.info.Name == "struct" || lexer.info.Name == "comment" {
				tc.RiType = "string"
				tc.RiName = lexer.info.Name
			} else {

				if lexer.count < len(lexer.info.Arguments) {
					tc.RiType = lexer.info.Arguments[lexer.count].Type
					tc.RiName = lexer.info.Arguments[lexer.count].Name
				} else {
					if !lexer.info.Parameterlist {
						tc.RiError = "invalid"
					}
					if len(tc.Tokens) > 1 {
						if tc.Tokens[1].RiType == "number" {
							tc.RiType = "float*" /* general */
						} else {
							tc.RiType = "token*"
						}
					}
					tc.RiName = fmt.Sprintf("_plist(%d)_", lexer.count)
				}

			}
			break
		case TokenLiteral:
			if lexer.info == nil {
				continue
			}
			if lexer.count < len(lexer.info.Arguments) {
				tc.RiType = lexer.info.Arguments[lexer.count].Type
				tc.RiName = lexer.info.Arguments[lexer.count].Name
			} else {
				if !lexer.info.Parameterlist {
					tc.RiError = "invalid"
				}
				tc.RiType = "token"
				tc.RiName = fmt.Sprintf("_plist(%d)_", lexer.count)
			}

			break
		default:
			if lexer.info == nil {
				continue
			}
			if lexer.count < len(lexer.info.Arguments) {
				tc.RiType = lexer.info.Arguments[lexer.count].Type
				tc.RiName = lexer.info.Arguments[lexer.count].Name
			} else {
				if !lexer.info.Parameterlist {
					tc.RiError = "invalid"
				}
				tc.RiType = "float" /* TODO: make a better guess */
				tc.RiName = fmt.Sprintf("_plist(%d)_", lexer.count)
			}
			break
		}

		lexer.count++
		out_token_stream = append(out_token_stream, tc)
	}

	return out_token_stream, nil
}
