package ric

type ProcessHandle struct {
}

/* RicFlush */
func (ric *ProcessHandle) RicFlush(marker string, synchronous bool, flushmode string) {

}

/* RicGetProgress */
func (ric *ProcessHandle) RicGetProgress() int {

	return 100
}

/* RicProcessCallbacks */
func (ric *ProcessHandle) RicProcessCallbacks() {}

/* RicOption */
func (ric *ProcessHandle) RicOption(name string, value interface{}) {

}

func New() *ProcessHandle {

	p := &ProcessHandle{}

	return p
}
