package rib

import (
	"bufio"
	"bytes"
	"gitlab.com/mae.earth/rigo/ri"
	"io"
)

type RIBParser2 int

func (p *RIBParser2) Parse(ctx *ri.RtContextHandle, callback ri.RtArchiveCallback, stream io.Reader) error {

	if stream == nil {
		return ErrInvalidStream
	}

	parser := NewParser()
	tokeniser := NewTokeniser()
	lexer := NewLexer()
	analyser := NewAnalyser()

	runner, err := NewRunner2(ctx, false)
	if err != nil {
		return err
	}
	runner.callback = callback

	scanner := bufio.NewScanner(stream)

	for scanner.Scan() {

		buf := scanner.Bytes()
		buf = append(buf, byte('\n'))

		tokens, err := continue_parser(parser, buf)
		if err != nil {
			return err
		}

		composites, err := continue_tokeniser(tokeniser, tokens)
		if err != nil {
			return err
		}

		out, err := continue_lexer(lexer, composites)
		if err != nil {
			return err
		}

		out, err = continue_analyser(analyser, out)
		if err != nil {
			return err
		}

		if err := continue_runner2(runner, out); err != nil {
			return err
		}
	}

	/* insert EOF token */

	return continue_runner2(runner, []*token_composite{{RiType: "EOF", RiName: "End of line user.", Value: int64(1)}})
}

type RIBParser int

func (p *RIBParser) Parse(ctx *ri.ContextHandle, callback ri.ArchiveCallback, stream io.Reader) error {

	if stream == nil {
		return ErrInvalidStream
	}

	parser := NewParser()
	tokeniser := NewTokeniser()
	lexer := NewLexer()
	analyser := NewAnalyser()

	runner, err := NewRunner(ctx, false)
	if err != nil {
		return err
	}
	runner.callback = callback

	scanner := bufio.NewScanner(stream)

	for scanner.Scan() {

		buf := scanner.Bytes()
		buf = append(buf, byte('\n'))

		tokens, err := continue_parser(parser, buf)
		if err != nil {
			return err
		}

		composites, err := continue_tokeniser(tokeniser, tokens)
		if err != nil {
			return err
		}

		out, err := continue_lexer(lexer, composites)
		if err != nil {
			return err
		}

		out, err = continue_analyser(analyser, out)
		if err != nil {
			return err
		}

		if err := continue_runner(runner, out); err != nil {
			return err
		}
	}

	/* insert EOF token */

	return continue_runner(runner, []*token_composite{{RiType: "EOF", RiName: "End of line user.", Value: int64(1)}})
}

func Fuzz(data []byte) int {

	errorhandler := func(code, severity int, msg string) { /* ignore */ } /* we are only interested in internal crashes and panics */

	ctx := ri.New()
	ctx.Begin("out.rib")
	ctx.ErrorHandler(errorhandler)
	defer ctx.End()

	parser := new(RIBParser)

	if err := parser.Parse(ctx, nil, bytes.NewReader(data)); err != nil {
		panic(err.Error())
	}

	return 1
}
