package rigo

import (
	"fmt"

	"gitlab.com/mae.earth/rigo/ri"
	/*"gitlab.com/mae.earth/rigo/rib" */
	"gitlab.com/mae.earth/rigo/ric"
)

/* Error */
type Error struct {
	Code     int
	Severity int
	Message  string
}

/* Error */
func (err Error) Error() string {
	return fmt.Sprintf("%03d:%05d %s", err.Severity, err.Code, err.Message)
}

/* ErrorHandlerFunc */
func ErrorHandlerFunc(f func(Error)) ri.ErrorHandlerFunc {
	return func(code, severity int, msg string) {
		if f != nil {
			f(Error{Code: code, Severity: severity, Message: msg})
		}
	}
}

/* Client */
type Client struct {
	*ri.RiWrapper
	*ric.ProcessHandle

	errorfunc ri.ErrorHandlerFunc
}

/* ErrorHandler */
func (c *Client) ErrorHandler(handler ri.ErrorHandlerFunc) {

	c.errorfunc = handler
	c.RiWrapper.ErrorHandler(handler)
}

/* New */
func New() *Client {

	client := &Client{}
	client.RiWrapper = ri.NewRiWrapper()
	client.errorfunc = ri.ErrorAbort

	client.ProcessHandle = ric.New( /* FIXME: add an interface here to interact with, from ContextHandle */ )

	return client
}
