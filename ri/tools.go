package ri

import (
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"io"

	"strconv"
)

/* TODO: make this an interfacer type */
func GenHandle(name, typeof string) (string, error) {

	if name == "" || name == "-" {

		b := make([]byte, 8)
		n, err := io.ReadFull(rand.Reader, b)
		if err != nil {
			return "-", err
		}

		if typeof == "" || typeof == "-" {
			return hex.EncodeToString(b[:n]), nil
		}

		return typeof + "_" + hex.EncodeToString(b[:n]), nil
	}

	return name, nil
}

/* Format or reduce RtFloat to a nice
 * compact repesentation for RIB
 */
func reduce(f float64) string {

	str := fmt.Sprintf("%f", f)
	s := 0
	neg := false
	for i, c := range str {
		if c != '0' {
			if c == '-' {
				neg = true
				continue
			}
			s = i
			break
		}
		if c == '.' {
			break
		}
	}

	e := 0
	for i := len(str) - 1; i >= 0; i-- {
		if str[i] != '0' {
			e = i + 1
			break
		}
		if str[i] == '.' {
			break
		}
	}

	str = str[s:e]
	if str == "." {
		str = "0"
	}
	if neg {
		str = "-" + str
	}
	if str[len(str)-1] == '.' {
		str = str[:len(str)-1]
	}

	return str
}

/* Specification */
type Specification struct {
	Class string
	Type  string
	Name  string
	N     int
}

/* IsJustName */
func (spec Specification) IsJustName() bool {
	return (spec.Type == "-")
}

/* Token */
func (spec Specification) Token() string {
	if spec.Class == "uniform" {
		/* ignore because it is the default */
		if spec.N > 1 {
			return fmt.Sprintf("%s[%d] %s", spec.Type, spec.N, spec.Name)
		}
		return fmt.Sprintf("%s %s", spec.Type, spec.Name)
	}

	if spec.N > 1 {
		return fmt.Sprintf("%s %s[%d] %s", spec.Class, spec.Type, spec.N, spec.Name)
	}

	return fmt.Sprintf("%s %s %s", spec.Class, spec.Type, spec.Name)
}

/* String */
func (spec Specification) String() string {
	return spec.Token()
}

/* GoType */
func (spec Specification) GoType() string {

	N := func(tag string, n int) string {
		if n > 1 {
			return fmt.Sprintf("[%d]%s", n, tag)
		}
		if n < 0 {
			return fmt.Sprintf("[]%s", tag)
		}
		return tag
	}

	out := ""

	/* override if a reference */
	if spec.Class == "reference" {
		out = N("string", spec.N)
		return out
	}

	switch spec.Type {
	case "string", "lighthandle", "archivehandle", "objecthandle":
		out = N("string", spec.N)
		break
	case "point", "normal", "vector":
		out = N("float64", 3)
		break
	case "hpoint":
		out = N("float64", 4)
		break
	case "interval":
		out = N("float64", 2)
		break
	case "int", "integer":
		out = N("int", spec.N)
		break
	case "float":
		out = N("float64", spec.N)
		break
	case "boolean", "bool":
		out = N("bool", spec.N)
		break
	case "color", "colour":
		out = N("float64", -1)
		break
	case "matrix", "basis":
		out = N("float64", 16)
		break
	default:
		fmt.Printf("Spec GoType -- unknown (%s)\n", spec)
		break
	}

	return out
}

/* ParseTokenSpecification */
func ParseTokenSpecification(token string) (Specification, error) {
	/* Token spec := [class] type name
	   * where class is either "constant" | "uniform" | "varying" | "vertex"
	   * where type is a golang (C) underlying type or a RenderMan type
		 * see pg. 13
	*/

	class := "uniform"
	typeof := "-"
	name := ""
	n := 1

	if len(token) == 0 {
		return Specification{}, toerror(101, 3, "Invalid token -- empty")
	}

	/* first pass */
	parts := 0
	mode := "name"
	for i := 0; i < len(token); i++ {
		if token[i] == ' ' {
			parts++
			switch mode {
			case "name":
				typeof = name
				name = ""
				mode = "type"
				break
			case "type":
				class = typeof
				typeof = name
				name = ""
				mode = "class"
				break
				/* class is in error -- but handled later */
			}
			continue
		}

		name += string(token[i])
	}
	parts++

	if parts > 3 {
		return Specification{}, toerror(101, 3, "Invalid token")
	}

	/* check for array of type */
	if parts >= 2 {
		if typeof == "-" {
			return Specification{}, toerror(101, 3, "Invalid type")
		}

		array := ""
		for i := 0; i < len(typeof); i++ {
			if typeof[i] == '[' {
				array = typeof[i:]
				typeof = typeof[:i]
			}
		}

		if len(array) > 0 {
			if array[0] == '[' && array[len(array)-1] == ']' {

				ni, err := strconv.Atoi(array[1 : len(array)-1])
				if err != nil {
					return Specification{}, toerror(101, 3, err.Error())
				}
				n = ni

			} else {

				return Specification{}, toerror(101, 3, "Invalid typed array")
			}
		}

		switch typeof {
		case "string", "point", "normal", "hpoint", "interval", "int", "integer",
			"float", "boolean", "bool", "color", "vector", "matrix", "basis",
			"lighthandle", "archivehandle", "objecthandle":
			break
		default:
			return Specification{}, toerror(101, 3, fmt.Sprintf("Invalid Token Type -- \"%s\"", typeof))
			break
		}
	}

	/* validate class */
	if parts == 3 {
		switch class {
		case "constant", "uniform", "varying", "vertex", "reference":
			break
		default:
			return Specification{}, toerror(101, 3, fmt.Sprintf("Invalid Token Class -- \"%s\"", class))
			break
		}
	}

	return Specification{Class: class, Type: typeof, Name: name, N: n}, nil
}

/* toerror */
func toerror(code, severity int, msg string) error {

	return fmt.Errorf("%03d:%05d %s", severity, code, msg)
}
