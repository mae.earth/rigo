package rib

import (
	"bufio"
	"fmt"
	"io"

	"strconv"

	"gitlab.com/mae.earth/rigo/ri"
	"gitlab.com/mae.earth/rigo/utils"
)

/* TODO:
 * the ri prototypes, as machine generated from the header files is incorrect,
 * as the RIB version of the commands are generally not the exact same.
 *
 * Solution, hand annotate the functions for RIB entry, testing against the output
 * from prman. Then adjust the machine generation to take into account these annotations.
 */

type Parser struct {
	line  int
	pos   int
	word  string
	nonce int

	commands *utils.BloomFilter
}

func NewParser() *Parser {

	p := &Parser{}
	p.commands = ri.RiBloomFilter()
	return p
}

/* continue_parser */
func continue_parser(parser *Parser, content []byte) ([]*token, error) {

	tokens := make([]*token, 0)

	totoken := func(word string, line, pos, n int) *token {

		if parser.commands.IsMember(word) {
			return &token{Word: word, Type: TokenCommand, Line: line, Pos: pos, Nonce: n}
		}

		/* attempt to work out the Ri type */
		if _, err := strconv.ParseFloat(word, 64); err != nil {

			return &token{Word: word, Type: TokenWord, RiType: "string", Line: line, Pos: pos, Nonce: n}
		}

		return &token{Word: word, Type: TokenWord, RiType: "number", Line: line, Pos: pos, Nonce: n}
	}

	last := func(tokens []*token) *token {
		if tokens == nil || len(tokens) == 0 {
			return nil
		}

		return tokens[len(tokens)-1]
	}

	print := func(tokens []*token) {
		if tokens == nil || len(tokens) == 0 {
			return
		}

		//fmt.Printf("%s\n", tokens[len(tokens)-1])
	}

	for _, c := range content {

		if c == '"' {
			if len(parser.word) > 0 {
				parser.nonce++
				tokens = append(tokens, totoken(parser.word, parser.line, parser.pos, parser.nonce))
				print(tokens)
				parser.word = ""
			}
			parser.nonce++
			tokens = append(tokens, &token{Type: TokenLiteral, Line: parser.line, Pos: parser.pos, Nonce: parser.nonce})
			print(tokens)
			continue
		}

		if c == ' ' || c == '\n' {
			if len(parser.word) > 0 {
				parser.nonce++
				t := totoken(parser.word, parser.line, parser.pos, parser.nonce)

				if c == '\n' && t.Type == TokenWord {
					t.Type = TokenLastWord
				}
				tokens = append(tokens, t)
				print(tokens)
				parser.word = ""
			}
			if c == '\n' {
				parser.line++
				parser.pos = 0
			}
			continue
		}

		if c == '#' {
			if tkn := last(tokens); tkn != nil {
				if tkn.Type == TokenArchive {
					tkn.RiType = "struct"
					continue
				}
			}
			parser.nonce++
			tokens = append(tokens, &token{Type: TokenArchive, Line: parser.line, Pos: parser.pos, Nonce: parser.nonce})
			print(tokens)
			continue
		}

		if c == '\t' {
			continue
		}

		if c == '[' {
			if len(parser.word) > 0 {
				parser.nonce++
				tokens = append(tokens, totoken(parser.word, parser.line, parser.pos, parser.nonce))
				print(tokens)
				parser.word = ""
			}
			parser.nonce++
			tokens = append(tokens, &token{Type: TokenOpen, Line: parser.line, Pos: parser.pos, Nonce: parser.nonce})
			print(tokens)
			continue
		}

		if c == ']' {
			if len(parser.word) > 0 {
				parser.nonce++
				tokens = append(tokens, totoken(parser.word, parser.line, parser.pos, parser.nonce))
				print(tokens)
				parser.word = ""
			}
			parser.nonce++
			tokens = append(tokens, &token{Type: TokenClose, Line: parser.line, Pos: parser.pos, Nonce: parser.nonce})
			print(tokens)
			continue
		}

		parser.word += string(c)
		parser.pos++
	} /* end of character range */

	return tokens, nil
}

func GeneralParser(stream io.Reader) error {

	if stream == nil {
		return ErrInvalidStream
	}

	parser := NewParser()
	tokeniser := NewTokeniser()
	lexer := NewLexer()
	analyser := NewAnalyser()

	ctx := ri.New()
	ctx.Begin("out.rib")
	defer ctx.End()

	runner, err := NewRunner(ctx, true)
	if err != nil {
		return err
	}

	scanner := bufio.NewScanner(stream)

	torun := make([]*token_composite, 0)

	for scanner.Scan() {

		buf := scanner.Bytes()
		buf = append(buf, byte('\n'))

		/* continue parser */
		tokens, err := continue_parser(parser, buf)
		if err != nil {
			return err
		}

		composites, err := continue_tokeniser(tokeniser, tokens)
		if err != nil {
			return err
		}

		out, err := continue_lexer(lexer, composites)
		if err != nil {
			return err
		}

		out, err = continue_analyser(analyser, out)
		if err != nil {
			return err
		}

		/*
			fmt.Printf("\n============================== stream =============================================================================================\n")
			fmt.Printf("token\t line:pos-> line:pos\t        Ri·type              Ri·name                 type : tokens{...}\n")
			fmt.Printf("-----------------------------------------------------------------------------------------------------------------------------------\n")

			for i, tkn := range out {

				fmt.Printf("%05d\t%s\n", i+1, tkn)
			}
			fmt.Printf("===================================================================================================================================\n")
		*/
		torun = append(torun, out...)

		if err := continue_runner(runner, torun); err != nil {
			return err
		}

		torun = make([]*token_composite, 0)
	}

	if err := continue_runner(runner, []*token_composite{{RiType: "EOF", RiName: "End of line user.", Value: int64(1)}}); err != nil {
		return err
	}

	if len(analyser.misunderstood) > 0 {
		fmt.Printf("\n\n============================== misunderstood stream =============================================================================\n")
		fmt.Printf("token\t line:pos-> line:pos\t        Ri·type              Ri·name                 type : tokens{...}\n")
		fmt.Printf("-----------------------------------------------------------------------------------------------------------------------------------\n")

		for i, tkn := range analyser.misunderstood {

			fmt.Printf("%05d\t%s\n", i+1, tkn)
		}
		fmt.Printf("===================================================================================================================================\n")
	}

	if len(analyser.errors) > 0 {

		fmt.Printf("\n\n============================== error stream =============================================================================\n")
		fmt.Printf("token\t line:pos-> line:pos\t        Ri·type              Ri·name                 type : tokens{...}\n")
		fmt.Printf("-----------------------------------------------------------------------------------------------------------------------------------\n")

		for i, tkn := range analyser.errors {

			fmt.Printf("%05d\t%s\n", i+1, tkn)
		}
		fmt.Printf("===================================================================================================================================\n")
	}

	return nil
}

func SpecificParser(ctx *ri.ContextHandle, stream io.Reader) error {

	if stream == nil {
		return ErrInvalidStream
	}

	parser := NewParser()
	tokeniser := NewTokeniser()
	lexer := NewLexer()
	analyser := NewAnalyser()

	runner, err := NewRunner(ctx, false)
	if err != nil {
		return err
	}

	scanner := bufio.NewScanner(stream)

	torun := make([]*token_composite, 0)

	for scanner.Scan() {

		buf := scanner.Bytes()
		buf = append(buf, byte('\n'))

		/* continue parser */
		tokens, err := continue_parser(parser, buf)
		if err != nil {
			return err
		}

		composites, err := continue_tokeniser(tokeniser, tokens)
		if err != nil {
			return err
		}

		out, err := continue_lexer(lexer, composites)
		if err != nil {
			return err
		}

		out, err = continue_analyser(analyser, out)
		if err != nil {
			return err
		}

		torun = append(torun, out...)

		if err := continue_runner(runner, torun); err != nil {
			return err
		}

		torun = make([]*token_composite, 0)
	}

	return continue_runner(runner, []*token_composite{{RiType: "EOF", RiName: "End of line user.", Value: int64(1)}})
}
