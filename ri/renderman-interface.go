/* machine generated
 * build tool for RiGO version 1
 * generated on 2017-04-24 13:55:03.189986827 +0000 UTC
 * source ./21.2/ri.h
 * RiVersion 5
 */

package ri

import (
	"fmt"
	"io"
	"os"
	"reflect"
	"strings"
	"sync"
)

/* Changelog :
 *
 * added custom code
 */

var w *RiWrapper

func init() {

	w = NewRiWrapper()
}

/* RiWrapper, protects the ContextHandle and provides the Renderman Interface */
type RiWrapper struct {
	mux sync.RWMutex
	ctx *RtContextHandle
}

/* ErrorHandler */
func (ri *RiWrapper) ErrorHandler(handler func(int, int, string)) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	ri.ctx.SetErrorHandler(handler)
}

/* ErrorHandler */
func ErrorHandler(handler func(int, int, string)) {
	w.ctx.SetErrorHandler(handler)
}

/* ArchiveBegin -- see  */
func (ri *RiWrapper) ArchiveBegin(name string, parameterlist ...interface{}) string {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	/* TODO: parse the params returned; "definition" is important, see documentation */

	/* if "definition" == "transient" then need to record the archive in the context, giving the handle for use
	 * otherwise for "persistent" we write straight-through and use the name as the handle (keep as the same)
	 */

	transient := true

	/* FIXME: need a much nicer way of doing this */
	for _, param := range params {
		if param.Token == "string definition" || param.Token == "definition" {

			switch param.Value.(RtString) {
			case RtString("persistent"):
				transient = false
				break
			case RtString("transient"):
				transient = true
				break
			}
		}
	}

	handleid := ""

	if !transient {
		handleid = name
	}

	handle, _err := GenHandle(handleid, "archive")
	if _err != nil {
		handleError(ri.ctx, &Error{Code: 101, Severity: 1, Msg: _err.Error()})
	}

	/* push onto the stack a new micro context */
	mctx := new(RtMicroContextHandle)
	mctx.name = name
	mctx.handle = handle
	mctx.persistent = !transient
	mctx.operations = make([]interface{}, 0)

	ri.ctx.micro_stack = append(ri.ctx.micro_stack, mctx)
	ri.ctx.micro_lookup[handle] = mctx

	if !transient {
		if err := process(ri.ctx, RiArchiveBegin{Name: RtToken(name), Params: params}); err != nil {
			handleError(ri.ctx, err)
		}
	}

	return handle
}

/* ArchiveBegin */
func ArchiveBegin(name string, parameterlist ...interface{}) string {
	return w.ArchiveBegin(name, parameterlist...)
}

/* ArchiveEnd */
func (ri *RiWrapper) ArchiveEnd() {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if len(ri.ctx.micro_stack) > 0 {

		mctx := ri.ctx.micro_stack[len(ri.ctx.micro_stack)-1]

		if mctx.persistent {
			if err := process(ri.ctx, RiArchiveEnd{}); err != nil {
				handleError(ri.ctx, err)
			}
		}

		ri.ctx.micro_stack = ri.ctx.micro_stack[:len(ri.ctx.micro_stack)-1]

		return
	}

	if err := process(ri.ctx, RiArchiveEnd{}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* ArchiveEnd */
func ArchiveEnd() {
	w.ArchiveEnd()
}

/* ArchiveRecord */
func (ri *RiWrapper) ArchiveRecord(typeof string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	out := ""
	format := ""

	/* pre-process the parameterlist, the first should be a string */
	if len(parameterlist) > 0 {

		str, ok := parameterlist[0].(string)
		if !ok {
			handleError(ri.ctx, &Error{Code: 101, Severity: 1, Msg: "expected string as first parameter"})
			return
		}
		format = str

		/* TODO: this is dangerous */
		out = fmt.Sprintf(str, parameterlist[1:]...)
	}

	/* do a special process here, we convert based on typeof, structure, comment, verbatim */
	switch strings.ToUpper(typeof) {
	case "STRUCTURE":

		if err := process(ri.ctx, RiArchiveRecordStructure{Out: RtUnescapedString(out), Format: format, Params: parameterlist[1:]}); err != nil {
			handleError(ri.ctx, err)
		}

		break
	case "COMMENT":

		if err := process(ri.ctx, RiArchiveRecordComment{Out: RtUnescapedString(out), Format: format, Params: parameterlist[1:]}); err != nil {
			handleError(ri.ctx, err)
		}

		break
	case "VERBATIM":

		if err := process(ri.ctx, RiArchiveRecordVerbatim{Out: RtUnescapedString(out), Format: format, Params: parameterlist[1:]}); err != nil {
			handleError(ri.ctx, err)
		}

		break
	default:

		handleError(ri.ctx, &Error{Code: 101, Severity: 1, Msg: fmt.Sprintf("unknown parameter \"%s\"", typeof)})
		break
	}

	/*
		if err := process(ri.ctx,RiArchiveRecord{ Type:RtToken(typeof),Params:params }); err != nil {
			handleError(ri.ctx,err)
		}*/
}

/* ArchiveRecord */
func ArchiveRecord(typeof string, parameterlist ...interface{}) {
	w.ArchiveRecord(typeof, parameterlist...)
}

/* AreaLightSource -- Depercated*/
func (ri *RiWrapper) AreaLightSource(name string, parameterlist ...interface{}) string {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiAreaLightSource{Name: RtToken(name), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}

	h, _err := GenHandle("", "light")
	if _err != nil {
		handleError(ri.ctx, &Error{Code: 101, Severity: 1, Msg: _err.Error()})
	}
	return h
}

/* Atmosphere */
func (ri *RiWrapper) Atmosphere(name string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiAtmosphere{Name: RtToken(name), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Atmosphere */
func Atmosphere(name string, parameterlist ...interface{}) {
	w.Atmosphere(name, parameterlist...)
}

/* Attribute */
func (ri *RiWrapper) Attribute(name string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiAttribute{Name: RtToken(name), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Attribute */
func Attribute(name string, parameterlist ...interface{}) {
	w.Attribute(name, parameterlist...)
}

/* AttributeBegin */
func (ri *RiWrapper) AttributeBegin() {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiAttributeBegin{}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* AttributeBegin */
func AttributeBegin() {
	w.AttributeBegin()
}

/* AttributeEnd */
func (ri *RiWrapper) AttributeEnd() {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiAttributeEnd{}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* AttributeEnd */
func AttributeEnd() {
	w.AttributeEnd()
}

/* Basis */
func (ri *RiWrapper) Basis(u [16]float64, ustep int, v [16]float64, vstep int) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiBasis{U: RtBasis(u), Ustep: RtInt(ustep), V: RtBasis(v), Vstep: RtInt(vstep)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Basis */
func Basis(u [16]float64, ustep int, v [16]float64, vstep int) {
	w.Basis(u, ustep, v, vstep)
}

/* Begin */
func (ri *RiWrapper) Begin(name interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if ri.ctx.driver != nil {
		handleError(ri.ctx, &Error{Code: 101, Severity: 1, Msg: "driver already started"})
	}

	/* TODO: check if the driver is open or not */
	if _, ok := name.(string); !ok {
		if _, ok := name.(io.Writer); !ok {
			handleError(ri.ctx, &Error{Code: 101, Severity: 1, Msg: "must be string or io.writer"})
		}
	}

	if str, ok := name.(string); ok {
		/* TODO: parse the string properly ... */
		if strings.HasSuffix(str, ".rib") {
			f, err := os.Create(str)
			if err != nil {
				handleError(ri.ctx, &Error{Code: 101, Severity: 1, Msg: err.Error()})
			} else {

				ri.ctx.driver = f
			}
		} else {

			/* TODO: this is where we should parse the user
			 * string and construct the required *driver pipeline*
			 */
			internal.RLock()
			defer internal.RUnlock()

			dname := "--" /* default */

			/* TODO: override the default name */

			if df, exists := internal.drivers[dname]; exists {

				ri.ctx.driver = df()

			} else {
				handleError(ri.ctx, &Error{Code: 101, Severity: 1, Msg: fmt.Sprintf("invalid %q driver", dname)})
			}
		}
	}

	if w, ok := name.(io.Writer); ok {

		ri.ctx.driver = &DriverWriter{w}
	}
}

/* Begin */
func Begin(name interface{}) {
	w.Begin(name)
}

/* Blobby */
func (ri *RiWrapper) Blobby(nleaf int, inst []int, flt []float64, str []string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiBlobby{Nleaf: RtInt(nleaf), Inst: []RtInt(toRtIntArray(inst)), Flt: []RtFloat(toRtFloatArray(flt)), Str: []RtToken(toRtTokenArray(str)), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Blobby */
func Blobby(nleaf int, inst []int, flt []float64, str []string, parameterlist ...interface{}) {
	w.Blobby(nleaf, inst, flt, str, parameterlist...)
}

/* Bound */
func (ri *RiWrapper) Bound(bound [6]float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiBound{Bound: RtBound(bound)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Bound */
func Bound(bound [6]float64) {
	w.Bound(bound)
}

/* Bxdf */
func (ri *RiWrapper) Bxdf(name, handle string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiBxdf{Name: RtToken(name), Handle: RtToken(handle), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Bxdf */
func Bxdf(name, handle string, parameterlist ...interface{}) {
	w.Bxdf(name, handle, parameterlist...)
}

/* Camera */
func (ri *RiWrapper) Camera(name string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiCamera{Name: RtToken(name), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Camera */
func Camera(name string, parameterlist ...interface{}) {
	w.Camera(name, parameterlist...)
}

/* Clipping */
func (ri *RiWrapper) Clipping(nearplane, farplane float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiClipping{Nearplane: RtFloat(nearplane), Farplane: RtFloat(farplane)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Clipping */
func Clipping(nearplane, farplane float64) {
	w.Clipping(nearplane, farplane)
}

/* ClippingPlane */
func (ri *RiWrapper) ClippingPlane(Nx, Ny, Nz, Px, Py, Pz float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiClippingPlane{Nx: RtFloat(Nx), Ny: RtFloat(Ny), Nz: RtFloat(Nz), Px: RtFloat(Px), Py: RtFloat(Py), Pz: RtFloat(Pz)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* ClippingPlane */
func ClippingPlane(Nx, Ny, Nz, Px, Py, Pz float64) {
	w.ClippingPlane(Nx, Ny, Nz, Px, Py, Pz)
}

/* Color */
func (ri *RiWrapper) Color(color []float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiColor{Color: []RtFloat(toRtFloatArray(color))}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Color */
func Color(color []float64) {
	w.Color(color)
}

/* ColorSamples */
func (ri *RiWrapper) ColorSamples(nRGB, RGBn []float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiColorSamples{NRGB: []RtFloat(toRtFloatArray(nRGB)), RGBn: []RtFloat(toRtFloatArray(RGBn))}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* ColorSamples */
func ColorSamples(nRGB, RGBn []float64) {
	w.ColorSamples(nRGB, RGBn)
}

/* ConcatTransform */
func (ri *RiWrapper) ConcatTransform(m [16]float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiConcatTransform{M: RtMatrix(m)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* ConcatTransform */
func ConcatTransform(m [16]float64) {
	w.ConcatTransform(m)
}

/* Cone */
func (ri *RiWrapper) Cone(height, radius, tmax float64, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiCone{Height: RtFloat(height), Radius: RtFloat(radius), Tmax: RtFloat(tmax), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Cone */
func Cone(height, radius, tmax float64, parameterlist ...interface{}) {
	w.Cone(height, radius, tmax, parameterlist...)
}

/* CoordSysTransform */
func (ri *RiWrapper) CoordSysTransform(fromspace string) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiCoordSysTransform{Fromspace: RtToken(fromspace)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* CoordSysTransform */
func CoordSysTransform(fromspace string) {
	w.CoordSysTransform(fromspace)
}

/* CoordinateSystem */
func (ri *RiWrapper) CoordinateSystem(name string) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiCoordinateSystem{Name: RtString(name)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* CoordinateSystem */
func CoordinateSystem(name string) {
	w.CoordinateSystem(name)
}

/* CropWindow */
func (ri *RiWrapper) CropWindow(left float64, right float64, bottom float64, top float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiCropWindow{Left: RtFloat(left), Right: RtFloat(right), Bottom: RtFloat(bottom), Top: RtFloat(top)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* CropWindow */
func CropWindow(left, right, bottom, top float64) {
	w.CropWindow(left, right, bottom, top)
}

/* Curves */
func (ri *RiWrapper) Curves(typeof string, nvertices []int, wrap string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiCurves{Type: RtToken(typeof), Nvertices: []RtInt(toRtIntArray(nvertices)), Wrap: RtToken(wrap), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Curves */
func Curves(typeof string, nvertices []int, wrap string, parameterlist ...interface{}) {
	w.Curves(typeof, nvertices, wrap, parameterlist...)
}

/* Cylinder */
func (ri *RiWrapper) Cylinder(radius float64, zmin float64, zmax float64, tmax float64, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiCylinder{Radius: RtFloat(radius), Zmin: RtFloat(zmin), Zmax: RtFloat(zmax), Tmax: RtFloat(tmax), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Cylinder */
func Cylinder(radius, zmin, zmax, tmax float64, parameterlist ...interface{}) {
	w.Cylinder(radius, zmin, zmax, tmax, parameterlist...)
}

/* Declare */
func (ri *RiWrapper) Declare(name string, decl string) string {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	token := decl + " " + name

	spec, err := ParseTokenSpecification(token)
	if err != nil {
		handleError(ri.ctx, &Error{Code: 101, Severity: 1, Msg: fmt.Sprintf("Declare parse spec error -- %v", err)})
	}

	if err := process(ri.ctx, RiDeclare{Name: RtString(name), Decl: RtString(decl)}); err != nil {
		handleError(ri.ctx, err)
	}

	ri.ctx.declarations[name] = spec.Token()

	return token
}

/* Declare */
func Declare(name, decl string) string {
	return w.Declare(name, decl)
}

/* Deformation */
func (ri *RiWrapper) Deformation(name string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiDeformation{Name: RtToken(name), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Deformation */
func Deformation(name string, parameterlist ...interface{}) {
	w.Deformation(name, parameterlist...)
}

/* DepthOfField */
func (ri *RiWrapper) DepthOfField(fstop float64, length float64, distance float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiDepthOfField{Fstop: RtFloat(fstop), Length: RtFloat(length), Distance: RtFloat(distance)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* DepthOfField */
func DepthOfField(fstop, length, distance float64) {
	w.DepthOfField(fstop, length, distance)
}

/* Detail */
func (ri *RiWrapper) Detail(bound [6]float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiDetail{Bound: RtBound(bound)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Detail */
func Detail(bound [6]float64) {
	w.Detail(bound)
}

/* DetailRange */
func (ri *RiWrapper) DetailRange(minvis float64, lotrans float64, hitrans float64, maxvis float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiDetailRange{Minvis: RtFloat(minvis), Lotrans: RtFloat(lotrans), Hitrans: RtFloat(hitrans), Maxvis: RtFloat(maxvis)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* DetailRange */
func DetailRange(minvis, lotrans, hitrans, maxvis float64) {
	w.DetailRange(minvis, lotrans, hitrans, maxvis)
}

/* Disk */
func (ri *RiWrapper) Disk(height float64, radius float64, tmax float64, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiDisk{Height: RtFloat(height), Radius: RtFloat(radius), Tmax: RtFloat(tmax), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Disk */
func Disk(height, radius, tmax float64, parameterlist ...interface{}) {
	w.Disk(height, radius, tmax, parameterlist...)
}

/* Displace */
func (ri *RiWrapper) Displace(name string, handle string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiDisplace{Name: RtToken(name), Handle: RtToken(handle), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Displace */
func Displace(name string, handle string, parameterlist ...interface{}) {
	w.Displace(name, handle, parameterlist...)
}

/* Displacement */
func (ri *RiWrapper) Displacement(name string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiDisplacement{Name: RtToken(name), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Displacement */
func Displacement(name string, parameterlist ...interface{}) {
	w.Displacement(name, parameterlist...)
}

/* Display */
func (ri *RiWrapper) Display(name string, typeof string, mode string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiDisplay{Name: RtString(name), Type: RtToken(typeof), Mode: RtToken(mode), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Display */
func Display(name string, typeof string, mode string, parameterlist ...interface{}) {
	w.Display(name, typeof, mode, parameterlist...)
}

/* DisplayChannel */
func (ri *RiWrapper) DisplayChannel(channel string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiDisplayChannel{Channel: RtToken(channel), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* DisplayChannel */
func DisplayChannel(channel string, parameterlist ...interface{}) {
	w.DisplayChannel(channel, parameterlist...)
}

/* DisplayFilter */
func (ri *RiWrapper) DisplayFilter(name, handle string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiDisplayFilter{Name: RtToken(name), Handle: RtToken(handle), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* DisplayFilter */
func DisplayFilter(name, handle string, parameterlist ...interface{}) {
	w.DisplayFilter(name, handle, parameterlist...)
}

/* EditAttributeBegin */
func (ri *RiWrapper) EditAttributeBegin(name string) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiEditAttributeBegin{Name: RtToken(name)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* EditAttributeBegin */
func EditAttributeBegin(name string) {
	w.EditAttributeBegin(name)
}

/* EditAttributeEnd */
func (ri *RiWrapper) EditAttributeEnd() {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiEditAttributeEnd{}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* EditAttributeEnd */
func EditAttributeEnd() {
	w.EditAttributeEnd()
}

/* EditBegin */
func (ri *RiWrapper) EditBegin(name string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiEditBegin{Name: RtToken(name), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* EditBegin */
func EditBegin(name string, parameterlist ...interface{}) {
	w.EditBegin(name, parameterlist...)
}

/* EditEnd */
func (ri *RiWrapper) EditEnd() {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiEditEnd{}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* EditEnd */
func EditEnd() {
	w.EditEnd()
}

/* EditWorldBegin */
func (ri *RiWrapper) EditWorldBegin(name string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiEditWorldBegin{Name: RtToken(name), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* EditWorldBegin */
func EditWorldBegin(name string, parameterlist ...interface{}) {
	w.EditWorldBegin(name, parameterlist...)
}

/* EditWorldEnd */
func (ri *RiWrapper) EditWorldEnd() {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiEditWorldEnd{}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* EditWorldEnd */
func EditWorldEnd() {
	w.EditWorldEnd()
}

/* Else */
func (ri *RiWrapper) Else() {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiElse{}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Else */
func Else() {
	w.Else()
}

/* ElseIf */
func (ri *RiWrapper) ElseIf(expr string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiElseIf{Expr: RtString(expr), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* ElseIf */
func ElseIf(expr string, parameterlist ...interface{}) {
	w.ElseIf(expr, parameterlist...)
}

/* EnableLightFilter */
func (ri *RiWrapper) EnableLightFilter(light string, filter string, onoff bool) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiEnableLightFilter{Light: RtLightHandle(light), Filter: RtToken(filter), Onoff: RtBoolean(onoff)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* EnableLightFilter */
func EnableLightFilter(light, filter string, onoff bool) {
	w.EnableLightFilter(light, filter, onoff)
}

/* End */
func (ri *RiWrapper) End() {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if ri.ctx.driver != nil {
		ri.ctx.driver.Close()
	}

	ri.ctx.driver = nil
}

/* End */
func End() {
	w.End()
}

/* Exposure */
func (ri *RiWrapper) Exposure(gain, gamma float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiExposure{Gain: RtFloat(gain), Gamma: RtFloat(gamma)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Exposure */
func Exposure(gain, gamma float64) {
	w.Exposure(gain, gamma)
}

/* Exterior */
func (ri *RiWrapper) Exterior(name string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiExterior{Name: RtToken(name), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Exterior */
func Exterior(name string, parameterlist ...interface{}) {
	w.Exterior(name, parameterlist...)
}

/* Format */
func (ri *RiWrapper) Format(xres int, yres int, pixel_ar float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiFormat{Xres: RtInt(xres), Yres: RtInt(yres), Pixel_ar: RtFloat(pixel_ar)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Format */
func Format(xres, yres int, pixel_ar float64) {
	w.Format(xres, yres, pixel_ar)
}

/* FrameAspectRatio */
func (ri *RiWrapper) FrameAspectRatio(ar float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiFrameAspectRatio{Ar: RtFloat(ar)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* FrameAspectRatio */
func FrameAspectRatio(ar float64) {
	w.FrameAspectRatio(ar)
}

/* FrameBegin */
func (ri *RiWrapper) FrameBegin(frame int) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiFrameBegin{Frame: RtInt(frame)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* FrameBegin */
func FrameBegin(frame int) {
	w.FrameBegin(frame)
}

/* FrameEnd */
func (ri *RiWrapper) FrameEnd() {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiFrameEnd{}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* FrameEnd */
func FrameEnd() {
	w.FrameEnd()
}

/* GeneralPolygon */
func (ri *RiWrapper) GeneralPolygon(nverts []int, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiGeneralPolygon{Nverts: []RtInt(toRtIntArray(nverts)), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* GeneralPolygon */
func GeneralPolygon(nverts []int, parameterlist ...interface{}) {
	w.GeneralPolygon(nverts, parameterlist...)
}

/* GeometricApproximation */
func (ri *RiWrapper) GeometricApproximation(typeof string, value float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiGeometricApproximation{Type: RtToken(typeof), Value: RtFloat(value)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* GeometricApproximation */
func GeometricApproximation(typeof string, value float64) {
	w.GeometricApproximation(typeof, value)
}

/* Geometry */
func (ri *RiWrapper) Geometry(name string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiGeometry{Name: RtToken(name), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Geometry */
func Geometry(name string, parameterlist ...interface{}) {
	w.Geometry(name, parameterlist...)
}

/* Hider */
func (ri *RiWrapper) Hider(name string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiHider{Name: RtToken(name), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Hider */
func Hider(name string, parameterlist ...interface{}) {
	w.Hider(name, parameterlist...)
}

/* HierarchicalSubdivisionMesh */
func (ri *RiWrapper) HierarchicalSubdivisionMesh(mask string, nf int, nverts []int, verts []int, nt int, tags []string, nargs []int, intargs []int, floatargs []float64, stringargs []string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiHierarchicalSubdivisionMesh{Mask: RtToken(mask), Nf: RtInt(nf), Nverts: []RtInt(toRtIntArray(nverts)), Verts: []RtInt(toRtIntArray(verts)), Nt: RtInt(nt), Tags: []RtToken(toRtTokenArray(tags)), Nargs: []RtInt(toRtIntArray(nargs)), Intargs: []RtInt(toRtIntArray(intargs)), Floatargs: []RtFloat(toRtFloatArray(floatargs)), Stringargs: []RtToken(toRtTokenArray(stringargs)), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* HierarchicalSubdivisionMesh */
func HierarchicalSubdivisionMesh(mask string, nf int, nverts []int, verts []int, nt int, tags []string, nargs []int, intargs []int, floatargs []float64, stringargs []string, parameterlist ...interface{}) {
	w.HierarchicalSubdivisionMesh(mask, nf, nverts, verts, nt, tags, nargs, intargs, floatargs, stringargs, parameterlist...)
}

/* Hyperboloid */
func (ri *RiWrapper) Hyperboloid(point1 [3]float64, point2 [3]float64, tmax float64, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiHyperboloid{Point1: RtPoint(point1), Point2: RtPoint(point2), Tmax: RtFloat(tmax), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Hyperboloid */
func Hyperboloid(point1 [3]float64, point2 [3]float64, tmax float64, parameterlist ...interface{}) {
	w.Hyperboloid(point1, point2, tmax, parameterlist...)
}

/* Identity */
func (ri *RiWrapper) Identity() {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiIdentity{}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Identity */
func Identity() {
	w.Identity()
}

/* IfBegin */
func (ri *RiWrapper) IfBegin(expr string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiIfBegin{Expr: RtString(expr), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* IfBegin */
func IfBegin(expr string, parameterlist ...interface{}) {
	w.IfBegin(expr, parameterlist...)
}

/* IfEnd */
func (ri *RiWrapper) IfEnd() {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiIfEnd{}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* IfEnd */
func IfEnd() {
	w.IfEnd()
}

/* Illuminate */
func (ri *RiWrapper) Illuminate(light string, onoff bool) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiIlluminate{Light: RtLightHandle(light), Onoff: RtBoolean(onoff)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Illuminate */
func Illuminate(light string, onoff bool) {
	w.Illuminate(light, onoff)
}

/* Imager */
func (ri *RiWrapper) Imager(name string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiImager{Name: RtToken(name), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Imager */
func Imager(name string, parameterlist ...interface{}) {
	w.Imager(name, parameterlist...)
}

/* Integrator */
func (ri *RiWrapper) Integrator(name, handle string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiIntegrator{Name: RtToken(name), Handle: RtToken(handle), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Integrator */
func Integrator(name, handle string, parameterlist ...interface{}) {
	w.Integrator(name, handle, parameterlist...)
}

/* Interior */
func (ri *RiWrapper) Interior(name string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiInterior{Name: RtToken(name), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Interior */
func Interior(name string, parameterlist ...interface{}) {
	w.Interior(name, parameterlist...)
}

/* Light */
func (ri *RiWrapper) Light(name, handle string, parameterlist ...interface{}) string {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	handle, _err := GenHandle(handle, "light")
	if _err != nil {
		handleError(ri.ctx, &Error{Code: 101, Severity: 1, Msg: _err.Error()})
	}

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiLight{Name: RtToken(name), Handle: RtToken(handle), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}

	return handle
}

/* Light */
func Light(name, handle string, parameterlist ...interface{}) string {
	return w.Light(name, handle, parameterlist...)
}

/* LightFilter */
func (ri *RiWrapper) LightFilter(name, handle string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiLightFilter{Name: RtToken(name), Handle: RtToken(handle), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* LightFilter */
func LightFilter(name, handle string, parameterlist ...interface{}) {
	w.LightFilter(name, handle, parameterlist...)
}

/* LightSource -- Deprecated */
func (ri *RiWrapper) LightSource(name string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiLightSource{Name: RtToken(name), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* MakeBrickMap */
func (ri *RiWrapper) MakeBrickMap(nptcs int, bkm string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiMakeBrickMap{Nptcs: RtInt(nptcs), Bkm: RtString(bkm), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* MakeBrickMap */
func MakeBrickMap(nptcs int, bkm string, parameterlist ...interface{}) {
	w.MakeBrickMap(nptcs, bkm, parameterlist...)
}

/* MakeBump */
func (ri *RiWrapper) MakeBump(pic, text, swrap, twrap, filt string, swidth, twidth float64, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiMakeBump{Pic: RtString(pic), Text: RtString(text), Swrap: RtToken(swrap), Twrap: RtToken(twrap), Filt: RtFilterFunc(filt), Swidth: RtFloat(swidth), Twidth: RtFloat(twidth), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* MakeBump */
func MakeBump(pic, text, swrap, twrap, filt string, swidth, twidth float64, parameterlist ...interface{}) {
	w.MakeBump(pic, text, swrap, twrap, filt, swidth, twidth, parameterlist...)
}

/* MakeCubeFaceEnvironment */
func (ri *RiWrapper) MakeCubeFaceEnvironment(px string, nx string, py string, ny string, pz string, nz string, text string, fov float64, filt string, swidth float64, twidth float64, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiMakeCubeFaceEnvironment{Px: RtString(px), Nx: RtString(nx), Py: RtString(py), Ny: RtString(ny), Pz: RtString(pz), Nz: RtString(nz), Text: RtString(text), Fov: RtFloat(fov), Filt: RtFilterFunc(filt), Swidth: RtFloat(swidth), Twidth: RtFloat(twidth), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* MakeCubeFaceEnvironment */
func MakeCubeFaceEnvironment(px, nx, py, ny, pz, nz, text string, fov float64, filt string, swidth, twidth float64, parameterlist ...interface{}) {
	w.MakeCubeFaceEnvironment(px, nx, py, ny, pz, nz, text, fov, filt, swidth, twidth, parameterlist...)
}

/* MakeLatLongEnvironment */
func (ri *RiWrapper) MakeLatLongEnvironment(pic, text, filt string, swidth, twidth float64, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiMakeLatLongEnvironment{Pic: RtString(pic), Text: RtString(text), Filt: RtFilterFunc(filt), Swidth: RtFloat(swidth), Twidth: RtFloat(twidth), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* MakeLatLongEnvironment */
func MakeLatLongEnvironment(pic, text, filt string, swidth, twidth float64, parameterlist ...interface{}) {
	w.MakeLatLongEnvironment(pic, text, filt, swidth, twidth, parameterlist...)
}

/* MakeShadow */
func (ri *RiWrapper) MakeShadow(pic, text string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiMakeShadow{Pic: RtString(pic), Text: RtString(text), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* MakeShadow */
func MakeShadow(pic, text string, parameterlist ...interface{}) {
	w.MakeShadow(pic, text, parameterlist...)
}

/* MakeTexture */
func (ri *RiWrapper) MakeTexture(pic string, text string, swrap string, twrap string, filt string, swidth float64, twidth float64, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiMakeTexture{Pic: RtString(pic), Text: RtString(text), Swrap: RtToken(swrap), Twrap: RtToken(twrap), Filt: RtFilterFunc(filt), Swidth: RtFloat(swidth), Twidth: RtFloat(twidth), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* MakeTexture */
func MakeTexture(pic string, text string, swrap string, twrap string, filt string, swidth float64, twidth float64, parameterlist ...interface{}) {
	w.MakeTexture(pic, text, swrap, twrap, filt, swidth, twidth, parameterlist...)
}

/* Matte */
func (ri *RiWrapper) Matte(onoff bool) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiMatte{Onoff: RtBoolean(onoff)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Matte */
func Matte(onoff bool) {
	w.Matte(onoff)
}

/* MotionBegin */
func (ri *RiWrapper) MotionBegin(parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	out := []float64{}

	for _, param := range parameterlist {
		switch reflect.TypeOf(param).String() {
		case "int":
			out = append(out, float64(param.(int)))
			break
		case "float64":
			out = append(out, param.(float64))
			break
		case "[]float64":
			out = append(out, param.([]float64)...)
			break
		default:
			handleError(ri.ctx, &Error{Code: 101, Severity: 1, Msg: "expecting float64"})
			break
		}
	}

	if err := process(ri.ctx, RiMotionBegin{Points: []RtFloat(toRtFloatArray(out))}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* MotionBegin */
func MotionBegin(parameterlist ...interface{}) {
	w.MotionBegin(parameterlist...)
}

/* MotionEnd */
func (ri *RiWrapper) MotionEnd() {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiMotionEnd{}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* MotionEnd */
func MotionEnd() {
	w.MotionEnd()
}

/* NuPatch */
func (ri *RiWrapper) NuPatch(nu int, uorder int, uknot []float64, umin float64, umax float64, nv int, vorder int, vknot []float64, vmin float64, vmax float64, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiNuPatch{Nu: RtInt(nu), Uorder: RtInt(uorder), Uknot: []RtFloat(toRtFloatArray(uknot)), Umin: RtFloat(umin), Umax: RtFloat(umax), Nv: RtInt(nv), Vorder: RtInt(vorder), Vknot: []RtFloat(toRtFloatArray(vknot)), Vmin: RtFloat(vmin), Vmax: RtFloat(vmax), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* NuPatch */
func NuPatch(nu int, uorder int, uknot []float64, umin float64, umax float64, nv int, vorder int, vknot []float64, vmin float64, vmax float64, parameterlist ...interface{}) {
	w.NuPatch(nu, uorder, uknot, umin, umax, nv, vorder, vknot, vmin, vmax, parameterlist...)
}

/* ObjectBegin */
func (ri *RiWrapper) ObjectBegin() string {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	h, err := GenHandle("", "object")
	if err != nil {
		handleError(ri.ctx, &Error{Code: 101, Severity: 1, Msg: err.Error()})
	}

	if err := process(ri.ctx, RiObjectBegin{Handle: RtObjectHandle(h)}); err != nil {
		handleError(ri.ctx, err)
	}

	return h
}

/* ObjectBegin */
func ObjectBegin() string {
	return w.ObjectBegin()
}

/* ObjectEnd */
func (ri *RiWrapper) ObjectEnd() {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiObjectEnd{}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* ObjectEnd */
func ObjectEnd() {
	w.ObjectEnd()
}

/* ObjectInstance */
func (ri *RiWrapper) ObjectInstance(h string) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiObjectInstance{H: RtObjectHandle(h)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* ObjectInstance */
func ObjectInstance(h string) {
	w.ObjectInstance(h)
}

/* Opacity */
func (ri *RiWrapper) Opacity(color []float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiOpacity{Color: []RtFloat(toRtFloatArray(color))}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Opacity */
func Opacity(color []float64) {
	w.Opacity(color)
}

/* Option */
func (ri *RiWrapper) Option(name string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiOption{Name: RtToken(name), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Option */
func Option(name string, parameterlist ...interface{}) {
	w.Option(name, parameterlist...)
}

/* Orientation */
func (ri *RiWrapper) Orientation(orient string) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiOrientation{Orient: RtToken(orient)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Orientation */
func Orientation(orient string) {
	w.Orientation(orient)
}

/* Paraboloid */
func (ri *RiWrapper) Paraboloid(radius, zmin, zmax, tmax float64, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiParaboloid{Radius: RtFloat(radius), Zmin: RtFloat(zmin), Zmax: RtFloat(zmax), Tmax: RtFloat(tmax), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Paraboloid */
func Paraboloid(radius, zmin, zmax, tmax float64, parameterlist ...interface{}) {
	w.Paraboloid(radius, zmin, zmax, tmax, parameterlist...)
}

/* Patch */
func (ri *RiWrapper) Patch(typeof string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiPatch{Type: RtToken(typeof), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Patch */
func Patch(typeof string, parameterlist ...interface{}) {
	w.Patch(typeof, parameterlist...)
}

/* PatchMesh */
func (ri *RiWrapper) PatchMesh(typeof string, nu int, uwrap string, nv int, vwrap string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiPatchMesh{Type: RtToken(typeof), Nu: RtInt(nu), Uwrap: RtToken(uwrap), Nv: RtInt(nv), Vwrap: RtToken(vwrap), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* PatchMesh */
func PatchMesh(typeof string, nu int, uwrap string, nv int, vwrap string, parameterlist ...interface{}) {
	w.PatchMesh(typeof, nu, uwrap, nv, vwrap, parameterlist...)
}

/* Pattern */
func (ri *RiWrapper) Pattern(name, handle string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiPattern{Name: RtToken(name), Handle: RtToken(handle), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Pattern */
func Pattern(name, handle string, parameterlist ...interface{}) {
	w.Pattern(name, handle, parameterlist...)
}

/* Perspective */
func (ri *RiWrapper) Perspective(fov float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiPerspective{Fov: RtFloat(fov)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Perspective */
func Perspective(fov float64) {
	w.Perspective(fov)
}

/* PixelFilter */
func (ri *RiWrapper) PixelFilter(funcof string, xwidth, ywidth float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiPixelFilter{Func: RtFilterFunc(funcof), Xwidth: RtFloat(xwidth), Ywidth: RtFloat(ywidth)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* PixelFilter */
func PixelFilter(funcof string, xwidth, ywidth float64) {
	w.PixelFilter(funcof, xwidth, ywidth)
}

/* PixelSampleImager */
func (ri *RiWrapper) PixelSampleImager(name string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiPixelSampleImager{Name: RtToken(name), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* PixelSampleImager */
func PixelSampleImager(name string, parameterlist ...interface{}) {
	w.PixelSampleImager(name, parameterlist...)
}

/* PixelSamples */
func (ri *RiWrapper) PixelSamples(x, y float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiPixelSamples{X: RtFloat(x), Y: RtFloat(y)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* PixelSamples */
func PixelSamples(x, y float64) {
	w.PixelSamples(x, y)
}

/* PixelVariance */
func (ri *RiWrapper) PixelVariance(varof float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiPixelVariance{Var: RtFloat(varof)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* PixelVariance */
func PixelVariance(varof float64) {
	w.PixelVariance(varof)
}

/* Points */
func (ri *RiWrapper) Points(nverts int, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiPoints{Nverts: RtInt(nverts), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Points */
func Points(nverts int, parameterlist ...interface{}) {
	w.Points(nverts, parameterlist...)
}

/* PointsGeneralPolygons */
func (ri *RiWrapper) PointsGeneralPolygons(npolys int, nloops []int, nverts []int, verts []int, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiPointsGeneralPolygons{Npolys: RtInt(npolys), Nloops: []RtInt(toRtIntArray(nloops)), Nverts: []RtInt(toRtIntArray(nverts)), Verts: []RtInt(toRtIntArray(verts)), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* PointsGeneralPolygons */
func PointsGeneralPolygons(npolys int, nloops []int, nverts []int, verts []int, parameterlist ...interface{}) {
	w.PointsGeneralPolygons(npolys, nloops, nverts, verts, parameterlist...)
}

/* PointsPolygons */
func (ri *RiWrapper) PointsPolygons(npolys int, nverts []int, verts []int, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiPointsPolygons{Npolys: RtInt(npolys), Nverts: []RtInt(toRtIntArray(nverts)), Verts: []RtInt(toRtIntArray(verts)), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* PointsPolygons */
func PointsPolygons(npolys int, nverts []int, verts []int, parameterlist ...interface{}) {
	w.PointsPolygons(npolys, nverts, verts, parameterlist...)
}

/* Polygon */
func (ri *RiWrapper) Polygon(nverts int, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiPolygon{Nverts: RtInt(nverts), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Polygon */
func Polygon(nverts int, parameterlist ...interface{}) {
	w.Polygon(nverts, parameterlist...)
}

/* Procedural */
func (ri *RiWrapper) Procedural(data []string, bound [6]float64, sfunc, ffunc string) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	/* we don't set the free func for RIB */

	if err := process(ri.ctx, RiProcedural{SubdivFunc: RtProcSubdivFunc(sfunc), Data: toRtStringArray(data), Bound: RtBound(bound)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Procedural */
func Procedural(data []string, bound [6]float64, sfunc, ffunc string) {
	w.Procedural(data, bound, sfunc, ffunc)
}

/* Procedural2 */
func (ri *RiWrapper) Procedural2(parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiProcedural2{Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Procedural2 */
func Procedural2(parameterlist ...interface{}) {
	w.Procedural2(parameterlist...)
}

/* Projection */
func (ri *RiWrapper) Projection(name string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiProjection{Name: RtToken(name), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Projection */
func Projection(name string, parameterlist ...interface{}) {
	w.Projection(name, parameterlist...)
}

/* Quantize */
func (ri *RiWrapper) Quantize(typeof string, one int, min int, max int, dither float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiQuantize{Type: RtToken(typeof), One: RtInt(one), Min: RtInt(min), Max: RtInt(max), Dither: RtFloat(dither)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Quantize */
func Quantize(typeof string, one int, min int, max int, dither float64) {
	w.Quantize(typeof, one, min, max, dither)
}

/* ReadArchive */
func (ri *RiWrapper) ReadArchive(name string, callback RtArchiveCallback, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	/* attempt to lookup the archive in memory, then fail to simply writing it out */
	if mctx, ok := ri.ctx.micro_lookup[name]; ok {

		if mctx.persistent {
			if err := process(ri.ctx, RiReadArchive{Name: RtToken(name), Callback: callback, Params: params}); err != nil {
				handleError(ri.ctx, err)
			}
			return
		}

		old := ri.ctx.callback
		ri.ctx.callback = callback

		/* run through the operations */
		for _, op := range mctx.operations {

			if err := process(ri.ctx, op); err != nil {
				handleError(ri.ctx, err)
			}
		}

		ri.ctx.callback = old

		return
	}

	if err := process(ri.ctx, RiReadArchive{Name: RtToken(name), Callback: callback, Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* ReadArchive */
func ReadArchive(name string, callback RtArchiveCallback, parameterlist ...interface{}) {
	w.ReadArchive(name, callback, parameterlist...)
}

/* RelativeDetail */
func (ri *RiWrapper) RelativeDetail(rel float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiRelativeDetail{Rel: RtFloat(rel)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* RelativeDetail */
func RelativeDetail(rel float64) {
	w.RelativeDetail(rel)
}

/* Resource */
func (ri *RiWrapper) Resource(handle string, typeof string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiResource{Handle: RtToken(handle), Type: RtToken(typeof), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Resource */
func Resource(handle string, typeof string, parameterlist ...interface{}) {
	w.Resource(handle, typeof, parameterlist...)
}

/* ResourceBegin */
func (ri *RiWrapper) ResourceBegin() {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiResourceBegin{}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* ResourceBegin */
func ResourceBegin() {
	w.ResourceBegin()
}

/* ResourceEnd */
func (ri *RiWrapper) ResourceEnd() {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiResourceEnd{}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* ResourceEnd */
func ResourceEnd() {
	w.ResourceEnd()
}

/* ReverseOrientation */
func (ri *RiWrapper) ReverseOrientation() {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiReverseOrientation{}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* ReverseOrientation */
func ReverseOrientation() {
	w.ReverseOrientation()
}

/* Rotate */
func (ri *RiWrapper) Rotate(angle float64, dx float64, dy float64, dz float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiRotate{Angle: RtFloat(angle), Dx: RtFloat(dx), Dy: RtFloat(dy), Dz: RtFloat(dz)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Rotate */
func Rotate(angle, dx, dy, dz float64) {
	w.Rotate(angle, dx, dy, dz)
}

/* SampleFilter */
func (ri *RiWrapper) SampleFilter(name string, handle string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiSampleFilter{Name: RtToken(name), Handle: RtToken(handle), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* SampleFilter */
func SampleFilter(name, handle string, parameterlist ...interface{}) {
	w.SampleFilter(name, handle, parameterlist...)
}

/* Scale */
func (ri *RiWrapper) Scale(sx, sy, sz float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiScale{Sx: RtFloat(sx), Sy: RtFloat(sy), Sz: RtFloat(sz)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Scale */
func Scale(sx, sy, sz float64) {
	w.Scale(sx, sy, sz)
}

/* ScopedCoordinateSystem */
func (ri *RiWrapper) ScopedCoordinateSystem(name string) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiScopedCoordinateSystem{Name: RtString(name)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* ScopedCoordinateSystem */
func ScopedCoordinateSystem(name string) {
	w.ScopedCoordinateSystem(name)
}

/* ScreenWindow */
func (ri *RiWrapper) ScreenWindow(left float64, right float64, bottom float64, top float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiScreenWindow{Left: RtFloat(left), Right: RtFloat(right), Bottom: RtFloat(bottom), Top: RtFloat(top)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* ScreenWindow */
func ScreenWindow(left, right, bottom, top float64) {
	w.ScreenWindow(left, right, bottom, top)
}

/* Shader */
func (ri *RiWrapper) Shader(name string, handle string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiShader{Name: RtToken(name), Handle: RtToken(handle), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Shader */
func Shader(name, handle string, parameterlist ...interface{}) {
	w.Shader(name, handle, parameterlist...)
}

/* ShadingInterpolation */
func (ri *RiWrapper) ShadingInterpolation(typeof string) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiShadingInterpolation{Type: RtToken(typeof)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* ShadingInterpolation */
func ShadingInterpolation(typeof string) {
	w.ShadingInterpolation(typeof)
}

/* ShadingRate */
func (ri *RiWrapper) ShadingRate(size float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiShadingRate{Size: RtFloat(size)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* ShadingRate */
func ShadingRate(size float64) {
	w.ShadingRate(size)
}

/* Shutter */
func (ri *RiWrapper) Shutter(opentime float64, closetime float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiShutter{Opentime: RtFloat(opentime), Closetime: RtFloat(closetime)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Shutter */
func Shutter(opentime, closetime float64) {
	w.Shutter(opentime, closetime)
}

/* Sides */
func (ri *RiWrapper) Sides(n int) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiSides{N: RtInt(n)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Sides */
func Sides(n int) {
	w.Sides(n)
}

/* Skew */
func (ri *RiWrapper) Skew(angle, d1x, d1y, d1z, d2x, d2y, d2z float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiSkew{Angle: RtFloat(angle), D1x: RtFloat(d1x), D1y: RtFloat(d1y), D1z: RtFloat(d1z), D2x: RtFloat(d2x), D2y: RtFloat(d2y), D2z: RtFloat(d2z)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Skew */
func Skew(angle, d1x, d1y, d1z, d2x, d2y, d2z float64) {
	w.Skew(angle, d1x, d1y, d1z, d2x, d2y, d2z)
}

/* SolidBegin */
func (ri *RiWrapper) SolidBegin(op string) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiSolidBegin{Op: RtString(op)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* SolidBegin */
func SolidBegin(op string) {
	w.SolidBegin(op)
}

/* SolidEnd */
func (ri *RiWrapper) SolidEnd() {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiSolidEnd{}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* SolidEnd */
func SolidEnd() {
	w.SolidEnd()
}

/* Sphere */
func (ri *RiWrapper) Sphere(radius float64, zmin float64, zmax float64, tmax float64, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiSphere{Radius: RtFloat(radius), Zmin: RtFloat(zmin), Zmax: RtFloat(zmax), Tmax: RtFloat(tmax), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Sphere */
func Sphere(radius, zmin, zmax, tmax float64, parameterlist ...interface{}) {
	w.Sphere(radius, zmin, zmax, tmax, parameterlist...)
}

/* SubdivisionMesh */
func (ri *RiWrapper) SubdivisionMesh(mask string, nf int, nverts []int, verts []int, nt int, tags []string, nargs []int, intargs []int, floatargs []float64, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiSubdivisionMesh{Mask: RtToken(mask), Nf: RtInt(nf), Nverts: []RtInt(toRtIntArray(nverts)), Verts: []RtInt(toRtIntArray(verts)), Nt: RtInt(nt), Tags: []RtToken(toRtTokenArray(tags)), Nargs: []RtInt(toRtIntArray(nargs)), Intargs: []RtInt(toRtIntArray(intargs)), Floatargs: []RtFloat(toRtFloatArray(floatargs)), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* SubdivisionMesh */
func SubdivisionMesh(mask string, nf int, nverts []int, verts []int, nt int, tags []string, nargs []int, intargs []int, floatargs []float64, parameterlist ...interface{}) {
	w.SubdivisionMesh(mask, nf, nverts, verts, nt, tags, nargs, intargs, floatargs, parameterlist...)
}

/* Surface */
func (ri *RiWrapper) Surface(name string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiSurface{Name: RtToken(name), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Surface */
func Surface(name string, parameterlist ...interface{}) {
	w.Surface(name, parameterlist...)
}

/* System */
func (ri *RiWrapper) System(name string) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiSystem{Name: RtString(name)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* System */
func System(name string) {
	w.System(name)
}

/* TextureCoordinates */
func (ri *RiWrapper) TextureCoordinates(s1 float64, t1 float64, s2 float64, t2 float64, s3 float64, t3 float64, s4 float64, t4 float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiTextureCoordinates{S1: RtFloat(s1), T1: RtFloat(t1), S2: RtFloat(s2), T2: RtFloat(t2), S3: RtFloat(s3), T3: RtFloat(t3), S4: RtFloat(s4), T4: RtFloat(t4)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* TextureCoordinates */
func TextureCoordinates(s1 float64, t1 float64, s2 float64, t2 float64, s3 float64, t3 float64, s4 float64, t4 float64) {
	w.TextureCoordinates(s1, t1, s2, t2, s3, t3, s4, t4)
}

/* Torus */
func (ri *RiWrapper) Torus(majrad float64, minrad float64, phimin float64, phimax float64, tmax float64, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiTorus{Majrad: RtFloat(majrad), Minrad: RtFloat(minrad), Phimin: RtFloat(phimin), Phimax: RtFloat(phimax), Tmax: RtFloat(tmax), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Torus */
func Torus(majrad float64, minrad float64, phimin float64, phimax float64, tmax float64, parameterlist ...interface{}) {
	w.Torus(majrad, minrad, phimin, phimax, tmax, parameterlist...)
}

/* Transform */
func (ri *RiWrapper) Transform(m [16]float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiTransform{M: RtMatrix(m)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Transform */
func Transform(m [16]float64) {
	w.Transform(m)
}

/* TransformBegin */
func (ri *RiWrapper) TransformBegin() {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiTransformBegin{}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* TransformBegin */
func TransformBegin() {
	w.TransformBegin()
}

/* TransformEnd */
func (ri *RiWrapper) TransformEnd() {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiTransformEnd{}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* TransformEnd */
func TransformEnd() {
	w.TransformEnd()
}

/* TransformPoints */
func (ri *RiWrapper) TransformPoints(fromspace string, tospace string, n int, points [][3]float64) []float64 {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	/*
		if err := process(ri.ctx,RiTransformPoints{ Fromspace:RtToken(fromspace),Tospace:RtToken(tospace),N:RtInt(n),Points:[]RtPoint(toRtPointArray(points)) }); err != nil {
			handleError(ri.ctx,err)
		} */

	return []float64{} /* FIXME TODO */
}

/* TransformPoints */
func TransformPoints(fromspace string, tospace string, n int, points [][3]float64) []float64 {
	return w.TransformPoints(fromspace, tospace, n, points)
}

/* Translate */
func (ri *RiWrapper) Translate(dx float64, dy float64, dz float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiTranslate{Dx: RtFloat(dx), Dy: RtFloat(dy), Dz: RtFloat(dz)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Translate */
func Translate(dx float64, dy float64, dz float64) {
	w.Translate(dx, dy, dz)
}

/* TrimCurve */
func (ri *RiWrapper) TrimCurve(nloops int, ncurves []int, order []int, knot []float64, min []float64, max []float64, n []int, u []float64, v []float64, w []float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiTrimCurve{Nloops: RtInt(nloops), Ncurves: []RtInt(toRtIntArray(ncurves)), Order: []RtInt(toRtIntArray(order)), Knot: []RtFloat(toRtFloatArray(knot)), Min: []RtFloat(toRtFloatArray(min)), Max: []RtFloat(toRtFloatArray(max)), N: []RtInt(toRtIntArray(n)), U: []RtFloat(toRtFloatArray(u)), V: []RtFloat(toRtFloatArray(v)), W: []RtFloat(toRtFloatArray(w))}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* TrimCurve */
func TrimCurve(nloops int, ncurves []int, order []int, knot []float64, min []float64, max []float64, n []int, u []float64, v []float64, lw []float64) {
	w.TrimCurve(nloops, ncurves, order, knot, min, max, n, u, v, lw)
}

/* VArchiveRecord */
func (ri *RiWrapper) VArchiveRecord(typeof string) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiVArchiveRecord{Type: RtToken(typeof)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* VArchiveRecord */
func VArchiveRecord(typeof string) {
	w.VArchiveRecord(typeof)
}

/* VPAtmosphere */
func (ri *RiWrapper) VPAtmosphere(name string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiVPAtmosphere{Name: RtToken(name), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* VPAtmosphere */
func VPAtmosphere(name string, parameterlist ...interface{}) {
	w.VPAtmosphere(name, parameterlist...)
}

/* VPInterior */
func (ri *RiWrapper) VPInterior(name string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiVPInterior{Name: RtToken(name), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* VPInterior */
func VPInterior(name string, parameterlist ...interface{}) {
	w.VPInterior(name, parameterlist...)
}

/* VPSurface */
func (ri *RiWrapper) VPSurface(name string, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiVPSurface{Name: RtToken(name), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* VPSurface */
func VPSurface(name string, parameterlist ...interface{}) {
	w.VPSurface(name, parameterlist...)
}

/* Volume */
func (ri *RiWrapper) Volume(typeof string, bound [6]float64, dimensions []int, parameterlist ...interface{}) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	params, err := parse(ri.ctx, parameterlist)
	if err != nil {
		handleError(ri.ctx, err)
	}

	if err := process(ri.ctx, RiVolume{Type: RtToken(typeof), Bound: RtBound(bound), Dimensions: []RtInt(toRtIntArray(dimensions)), Params: params}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* Volume */
func Volume(typeof string, bound [6]float64, dimensions []int, parameterlist ...interface{}) {
	w.Volume(typeof, bound, dimensions, parameterlist...)
}

/* VolumePixelSamples */
func (ri *RiWrapper) VolumePixelSamples(x, y float64) {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiVolumePixelSamples{X: RtFloat(x), Y: RtFloat(y)}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* VolumePixelSamples */
func VolumePixelSamples(x, y float64) {
	w.VolumePixelSamples(x, y)
}

/* WorldBegin */
func (ri *RiWrapper) WorldBegin() {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiWorldBegin{}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* WorldBegin */
func WorldBegin() {
	w.WorldBegin()
}

/* WorldEnd */
func (ri *RiWrapper) WorldEnd() {
	ri.mux.Lock()
	defer ri.mux.Unlock()

	if err := process(ri.ctx, RiWorldEnd{}); err != nil {
		handleError(ri.ctx, err)
	}
}

/* WorldEnd */
func WorldEnd() {
	w.WorldEnd()
}

/* EOF */
