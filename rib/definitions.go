package rib

import (
	"errors"
	"fmt"
	"reflect"
)

type RiWriter interface {
	V(fname string, args, tokens, values []interface{}) error
}

var (
	ErrInvalidStream = errors.New("Invalid Stream")
)

const (
	TokenLastWord = byte(0)
	TokenWord     = byte(1)
	TokenLiteral  = byte(2)
	TokenOpen     = byte(3)
	TokenClose    = byte(4)
	TokenEOF      = byte(5)

	TokenCommand = byte(128)
	TokenArchive = byte(129)
)

type token_composite struct {
	RiType  string
	RiName  string /* _plist_ is reserved for parameters in the parameterlist (based on prototype) */
	RiError string
	Value   interface{}

	Tokens []*token
}

func (tc *token_composite) String() string {
	if tc.Tokens == nil || len(tc.Tokens) == 0 {
		return "empty"
	}

	out := ""
	for _, tkn := range tc.Tokens {
		if len(out) > 0 {
			out += " "
		}
		out += tkn.ShortString()
	}

	if tc.RiError != "" {
		if len(out) > 0 {
			out += " "
		}
		out += "error(" + tc.RiError + ")"
	}

	value := "nil"
	if tc.Value != nil {
		value = reflect.TypeOf(tc.Value).String()
	}

	return fmt.Sprintf("%05d:%03d->%05d:%03d\t%15s %20s %20s | %s", tc.Tokens[0].Line, tc.Tokens[0].Pos,
		tc.Tokens[len(tc.Tokens)-1].Line, tc.Tokens[len(tc.Tokens)-1].Pos,
		tc.RiType, tc.RiName, value, out)
}

type token struct {
	Type   byte
	RiType string
	Word   string
	Line   int
	Pos    int

	Nonce int
}

func (t *token) String() string {
	ty := "WORD"
	word := ""
	switch t.Type {
	case TokenLiteral:
		ty = "LITERAL"
		word = "---\""
		break
	case TokenOpen:
		ty = "OPEN"
		word = "---["
		break
	case TokenClose:
		ty = "CLOSE"
		word = "---]"
		break
	case TokenCommand:
		ty = "COMMAND"
		word = t.Word
		break
	case TokenEOF:
		ty = "EOF"
		word = "End of line user."
		break
	case TokenLastWord:
		ty = "WORD·LAST"
		word = t.Word
		break
	case TokenArchive:
		ty = "ARCHIVE"
		word = "comment"
		break
	default:
		word = t.Word
		break
	}
	return fmt.Sprintf("%05d:%03d\t%9s[%6s] %s", t.Line, t.Pos, ty, t.RiType, word)
}

func (t *token) ShortString() string {
	word := ""
	switch t.Type {
	case TokenLiteral:
		word = "\""
		break
	case TokenOpen:
		word = "["
		break
	case TokenClose:
		word = "]"
		break
	case TokenCommand:
		word = "C'" + t.Word + "'"
		break
	case TokenEOF:
		word = "EOF"
		break
	case TokenWord, TokenLastWord:
		word = "W'" + t.Word + "'"
		break
	case TokenArchive:
		word = "#"
		break
	default:
		word = t.Word
		break
	}

	return word
}
