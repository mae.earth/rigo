package rib

import (
	"fmt"
	"reflect"
	"strconv"

	"mae.earth/rigo/ri"
)

/* TODO:
 * the ri prototypes, as machine generated from the header files is incorrect,
 * as the RIB version of the commands are generally not the exact same.
 *
 * Solution, hand annotate the functions for RIB entry, testing against the output
 * from prman. Then adjust the machine generation to take into account these annotations.
 */

type Analyser struct {
	misunderstood []*token_composite
	errors        []*token_composite

	ctx *ri.RtContextHandle
}

func (a *Analyser) version(ver float64) error {

	if ver != ri.RIBVersion {
		return fmt.Errorf("Unable to parse this stream")
	}

	return nil
}

func NewAnalyser() *Analyser {

	return &Analyser{ctx: ri.New(), misunderstood: make([]*token_composite, 0), errors: make([]*token_composite, 0)}
}

func continue_analyser(analyser *Analyser, token_stream []*token_composite) ([]*token_composite, error) {

	ctx := reflect.ValueOf(analyser.ctx)

	/* parse token_composite to an array of tokens */
	parse_toarrayoftokens := func(tc *token_composite) ([]string, error) {

		if tc.RiType != "token*" && tc.RiType != "string*" {
			return nil, fmt.Errorf("expecting (token* | string*) type but got \"%s\" instead", tc.RiType)
		}

		array := make([]string, 0)

		word := ""
		literal := false

		for i := 0; i < len(tc.Tokens); i++ {

			switch tc.Tokens[i].Type {
			case TokenWord, TokenLastWord:
				if !literal {
					return array, fmt.Errorf("expecting literal character")
				}
				if len(word) > 0 {
					word += " "
				}
				word += tc.Tokens[i].Word
				break
			case TokenLiteral:
				if literal {
					array = append(array, word)
					word = ""
				}
				literal = !literal
				break
			}
		}

		return array, nil
	}

	for _, tc := range token_stream {

		/* for each composite work out the actual values and convert ready for injesting into the RiStream */
		switch tc.RiType {
		case "func":

			/* check for the special function 'version' */
			if tc.RiName == "version" {

				tc.Value = analyser.version
				tc.RiType = "internal·func"
			} else {

				/* attempt to find the function in the interface */
				f := ctx.MethodByName(tc.Tokens[0].Word)
				if !f.IsValid() {
					tc.RiError = "function not found"
				} else {
					tc.Value = f
				}
			}
			break
		case "string":
			if tc.RiName != "comment" && tc.RiName != "struct" {
				str := ""
				bracket := false

				if tc.Tokens[0].Type != TokenLiteral {
					tc.RiError = "invalid string, no opening literal"
				} else {

					if len(tc.Tokens) > 2 {

						for i := 1; i < len(tc.Tokens); i++ {

							if tc.Tokens[i].Type == TokenLiteral {
								break
							}

							switch tc.Tokens[i].Type {
							case TokenOpen:
								if bracket {
									tc.RiError = "embeddied open bracket"
								}
								bracket = true
								str += "["

								break
							case TokenClose:
								if !bracket {
									tc.RiError = "closed bracket when not open"
								}
								bracket = false
								str += "]"

								break
							case TokenWord, TokenLastWord:
								if len(str) > 0 && !bracket {
									str += " "
								}
								str += tc.Tokens[i].Word
								break
							}
						}
					}
				}

				tc.Value = str
			}
			break
		case "string*":
			array, err := parse_toarrayoftokens(tc)
			if err != nil {
				tc.RiError = err.Error()
			}
			if len(array) == 1 {
				tc.Value = array[0]
			} else {
				tc.Value = array
			}
			break
		case "token", "lighthandle", "objecthandle":

			str := ""
			bracket := false

			if tc.Tokens[0].Type != TokenLiteral {
				tc.RiError = "invalid token, no opening literal"
			} else {

				if len(tc.Tokens) > 2 {

					for i := 1; i < len(tc.Tokens); i++ {

						if tc.Tokens[i].Type == TokenLiteral {
							break
						}

						switch tc.Tokens[i].Type {
						case TokenOpen:
							if bracket {
								tc.RiError = "embeddied open bracket"
							}
							bracket = true
							str += "["

							break
						case TokenClose:
							if !bracket {
								tc.RiError = "closed bracket when not open"
							}
							bracket = false
							str += "]"

							break
						case TokenWord, TokenLastWord:
							if len(str) > 0 && !bracket {
								str += " "
							}
							str += tc.Tokens[i].Word
							break
						}
					}
				}
			}

			tc.Value = str
			break
		case "token*":

			array, err := parse_toarrayoftokens(tc)
			if err != nil {
				tc.RiError = err.Error()
			}
			/* special treatment of singular arrays */
			if len(array) == 1 {
				tc.Value = array[0]
			} else {
				tc.Value = array
			}
			break
		case "float":
			f, err := strconv.ParseFloat(tc.Tokens[0].Word, 64)
			if err != nil {
				tc.RiError = err.Error()
			}
			tc.Value = f
			break
		case "float*", "bound":

			if len(tc.Tokens) <= 1 {
				tc.RiError = "invalid array"
			} else {

				array := make([]float64, 0)

				if len(tc.Tokens) > 2 {

					for i := 1; i < len(tc.Tokens); i++ {
						if tc.Tokens[i].Type == TokenClose {
							break
						}

						f, err := strconv.ParseFloat(tc.Tokens[i].Word, 64)
						if err != nil {
							tc.RiError = err.Error()
							break
						}

						array = append(array, f)
					}
				}

				if tc.RiType == "bound" {
					if len(array) != 6 {
						tc.RiError = "invalid bound"
					} else {

						tc.Value = [6]float64{array[0], array[1], array[2], array[3], array[4], array[5]}
					}
				} else { /* default */

					tc.Value = array
				}
			}
			break
		case "int":
			i, err := strconv.ParseInt(tc.Tokens[0].Word, 10, 64)
			if err != nil {
				tc.RiError = err.Error()
			}
			tc.Value = int(i)
			break
		case "int*":

			if len(tc.Tokens) <= 1 {
				tc.RiError = "invalid array"
			} else {

				array := make([]int, 0)
				if len(tc.Tokens) > 2 {

					for i := 1; i < len(tc.Tokens); i++ {
						if tc.Tokens[i].Type == TokenClose {
							break
						}

						j, err := strconv.ParseInt(tc.Tokens[i].Word, 10, 64)
						if err != nil {
							tc.RiError = err.Error()
							break
						}

						array = append(array, int(j))
					}
				}

				tc.Value = array
			}
			break
		case "boolean":

			b := false

			i, err := strconv.ParseInt(tc.Tokens[0].Word, 10, 64)
			if err != nil {
				tc.RiError = err.Error()
			} else {

				if i != 0 {
					b = true
				}
			}

			tc.Value = b

			break
		default:
			analyser.misunderstood = append(analyser.misunderstood, tc)
			break
		}

		if len(tc.RiError) > 0 {
			analyser.errors = append(analyser.errors, tc)
		}
	}

	return token_stream, nil
}
