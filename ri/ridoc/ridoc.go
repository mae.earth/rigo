/* rigo/ri/ridoc/ridoc.go */
package main

import (
	"os"
	"fmt"
	"reflect"
	"strings"

	"mae.earth/rigo/ri"
)

/* TODO: 
 * 
 * some functions do have return values and these currently are not present 
 */

func main() {

	/* basic test search for RiLight */

	for _,searchfor := range os.Args[1:] {
	
		found := false

		var entry *Entry

		for _,s := range ri.All {

			t := reflect.TypeOf(s)
			for i := 0; i < t.NumField(); i++ {
				field := t.Field(i)
				tag := field.Tag.Get("ri")
	
				if tag == "-" {
					continue
				}
	
				if field.Name == "funcname" {
								
					if tag == searchfor || ("Ri" + tag) == searchfor {
	
						found = true
						break
					}
				}
			}
	
			if found {
	
				entry = &Entry{}
				entry.Arguments = make([]string,0)
	
				/* preprocess */
				for i := 0; i < t.NumField(); i++ {
					field := t.Field(i)
					tag := field.Tag.Get("ri")
	
					if tag == "-" {
						continue
					}
	
					switch field.Name {
						case "funcname":
							/* do nothing */
							entry.FuncName = tag
						break
						case "params","Params":
							entry.HasParameterlist = true
						break
	
						default:
	
							entry.Arguments = append(entry.Arguments, tag)
	
							//fmt.Printf("\t%02d/%02d %q = %q\n",i + 1,t.NumField(),field.Name,tag)
						break
					}
				}
	
				break			
			}
		}
	
		if entry != nil {
	
			fmt.Printf("%s\n",entry.FuncName)
	
			fmt.Printf("RIB........\t%s\n",entry.RIB())
			fmt.Printf("RIGO/GO....\t%s\n",entry.GO())
	
			/*
			for _,arg := range entry.Arguments {
	
				parts := strings.Split(arg,",")
				if len(parts) != 2 {
					continue
				}
	
				fmt.Printf("\t%q %s\n",parts[0],parts[1])
			}*/
		}
	}

	
}


type Entry struct {

	FuncName string

	Arguments []string
	HasParameterlist bool
}

func (e *Entry) RIB() string {

	out := fmt.Sprintf("%s ",e.FuncName)
	for _,arg := range e.Arguments {
		parts := strings.Split(arg,",")
		if len(parts) != 2 {
			continue
		}
		out += fmt.Sprintf("%s ",parts[0])
	}

	if e.HasParameterlist {
		out += "<parameterlist>"
	}


	return out
}

func (e *Entry) GO() string {

	out := fmt.Sprintf("ri.%s(",e.FuncName)
	for i,arg := range e.Arguments {
		parts := strings.Split(arg,",")
		if len(parts) != 2 {
			continue
		}
		if i > 1 {
			out += ", "
		}
		out += fmt.Sprintf("%s %s",parts[0],parts[1])
	}

	if e.HasParameterlist {
		if len(e.Arguments) > 0 {
			out += ", "
		}
		out += "parameterlist ...interface{}"
	}

	out += ")"

	return out
}









