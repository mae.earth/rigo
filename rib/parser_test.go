package rib

import (
	. "github.com/smartystreets/goconvey/convey"
	"testing"

	"strings"

	"gitlab.com/mae.earth/rigo/ri"
)

func Test_Parser(t *testing.T) {

	Convey("general parser", t, func() {

		So(GeneralParser(strings.NewReader(RIBExample0)), ShouldBeNil)

		//So(GeneralParser(strings.NewReader(RIBExample1)),ShouldBeNil)
	})
}

func Test_CompleteExample(t *testing.T) {

	Convey("general parser -- complete example", t, func() {

		So(GeneralParser(strings.NewReader(RIBCompleteExample0)), ShouldBeNil)
	})

	Convey("specific parser -- complete example", t, func() {

		ctx := ri.New()
		ctx.Begin("specific.rib")
		defer ctx.End()

		So(SpecificParser(ctx, strings.NewReader(RIBCompleteExample0)), ShouldBeNil)
	})

}

func Test_GarbageParsing(t *testing.T) {

	Convey("basic garbage parser", t, func() {

		ctx := ri.New()
		ctx.Begin("garbage.rib")
		defer ctx.End()

		So(SpecificParser(ctx, strings.NewReader(RIBGarbageExample)), ShouldBeNil)
	})
}

const RIBExample0 = `##RenderMan RIB-Structure 1.1
# this is a [bad] comment
version 3.04
Projection "perspective" "float fov" 30.0
Color [1 0 0]
Sphere 1 -1 1 360
`

const RIBExample1 = `##RenderMan RIB-Structure 1.1
version 3.04
Display "sphere.tif" "file" "rgb"
Format 320 240 1
Translate 0 0 6
WorldBegin
Projection "perspective" "float fov" [30.0]
Color [1 0 0]
Sphere 1 -1 1 360
WorldEnd`

const CON = `
(CONS
	(options "utf-8")
	(RIB
		(structure "RenderMan" "RIB-Structure" 1.1)
		(version 3.04)
		(Display "sphere.tif" "file" "rgb" (··· ("int test" 3)))
		(Format 320 340 1)
		(Translate 0 0 6)
		(World<Begin&End>
			(Projection "perspective" (··· ("float fov" 30.0)("float foo" 12.3)("color bar",(2 3 4))))
			(Color (' 1 0 0))
			(Sphere 1 -1 360))))


 (··· (...)) :- parameterlist, treat all cons within this structure as additional data
 (' ...) :- pure data cons

`
const RIBCompleteExample0 = `
##RenderMan RIB
version 3.04
Option "ribparse" "string varsubst" ["^"]
Option "user" "string jobid" ["13c5e3e3f8b958dee36041ba195b260701dd6f6a"] "string renderpass" ["beauty"] 
##RenderMan RIB
Declare "resolution" "constant float[2]"
Declare "baseColor" "color"
ArchiveBegin "lighting_1" 
AttributeBegin 
# this is our lights
Translate 5 5 5
Light "PxrEnvDayLight" "light1" 
Geometry "envsphere" "constant float[2] resolution" [1024 1024]
AttributeEnd 
ArchiveEnd 
ArchiveBegin "lighting_2" 
AttributeBegin 
Translate -5 5 -5
Light "PxrEnvDayLight" "light2" 
Geometry "envsphere" "constant float[2] resolution" [1024 1024]
AttributeEnd 
ArchiveEnd 
Display "test.exr" "openexr" "rgba" 
Format 640 480 1
Projection "perspective" "float fov" [30]
Hider "raytrace" "int maxsamples" [256] "int minsamples" [16] "int incremental" [100] "float[4] aperture" [0 0 0 0]
Integrator "PxrPathTracer" "production" 
Shutter 0 1
System "echo 'jobid=^{Option:user:jobid} renderpass=^{Option:user:renderpass} point=WorldBegin'"
WorldBegin 
ObjectBegin "object_1" 
Sphere 1 -1 1 360 
ObjectEnd 
ReadArchive "test2.rib" 
Translate 0 0 10
ReadArchive "lighting_1" 
Illuminate "light1" 1
ReadArchive "lighting_2" 
Illuminate "light2" 1
AttributeBegin 
MotionBegin 3  [0 0.5 1]
Translate 1 0 0
Translate 0 0 0
Translate -1 0 0
MotionEnd 
Bxdf "PxrDisney" "surface" "baseColor" [1 0 0]
ObjectInstance "object_1" 
AttributeEnd 
AttributeBegin 

this is just junk
so should be ignored

MotionBegin 5 [0 0.25 0.5 0.75 1]
Translate 0 1 0
Translate 0 0.5 0
Translate 0 0 0
Translate 0 -0.5 0
Translate 0 -1 0
MotionEnd 
Bxdf "PxrDisney" "surface" "baseColor" [0 0 1]
ObjectInstance "object_1" 
AttributeEnd 
AttributeBegin 
Bound [0 0 0 1 1 1]
Bxdf "PxrDisney" "surface_2" "color baseColor" [0 1 0]
Sphere 1 -1 1 360 
AttributeEnd 
WorldEnd 
System "echo 'jobid=^{Option:user:jobid} renderpass=^{Option:user:renderpass} point=WorldEnd'"`

const RIBGarbageExample = `
askdjasd lkjasdka jsdkasjf dksjdfksdjfkl jdfj
sadfo skdfpo sdfgop sdpogj sdogj posdjg pjssd 1.0
dsdf 2 43 2 [0 291 ] asd
Shutter 0 1
MotionBegin
MotionEnd

asd asd asd asee sdfcvlkj fwfei j 
Translate 1 0 asdasd kj 0
`
