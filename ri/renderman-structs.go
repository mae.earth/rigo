/* machine generated
 * build tool for RiGO version 1
 * generated on 2017-04-24 13:17:17.481889288 +0000 UTC
 * source ./21.2/ri.h
 * RiVersion 5
 */

package ri

/* RiArchiveBegin */
type RiArchiveBegin struct {
	funcname RtDecoration `ri:"ArchiveBegin"`
	depth    RtDecoration `ri:"1"`
	Name     RtToken      `ri:"name,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiArchiveEnd */
type RiArchiveEnd struct {
	depth    RtDecoration `ri:"-1"`
	funcname RtDecoration `ri:"ArchiveEnd"`
}

/* TODO: move these functions to a separate file to prevent overwriting later */
/* RiArchiveRecord -- generated but not used */
type RiArchiveRecord struct {
	funcname RtDecoration `ri:"ArchiveRecord"`
	Type     RtToken      `ri:"type,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiArchiveRecordStructure -- hand crafted */
type RiArchiveRecordStructure struct {
	funcname RtDecoration      `ri:"##"`
	Out      RtUnescapedString `ri:"out,string"`
	Format   string            `ri:"-"`
	Params   []interface{}     `ri:"-"`
}

/* RiArchiveRecordComment -- hand crafted */
type RiArchiveRecordComment struct {
	funcname RtDecoration      `ri:"#"`
	Out      RtUnescapedString `ri:"out,string"`
	Format   string            `ri:"-"`
	Params   []interface{}     `ri:"-"`
}

/* RtArchiveRecordVerbatim -- hand crafted */
type RiArchiveRecordVerbatim struct {
	funcname RtDecoration      `ri:""`
	Out      RtUnescapedString `ri:"out,string"`
	Format   string            `ri:"-"`
	Params   []interface{}     `ri:"-"`
}

/* RiAreaLightSource */
type RiAreaLightSource struct {
	funcname RtDecoration `ri:"AreaLightSource"`
	Name     RtToken      `ri:"name,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiAtmosphere */
type RiAtmosphere struct {
	funcname RtDecoration `ri:"Atmosphere"`
	Name     RtToken      `ri:"name,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiAttribute */
type RiAttribute struct {
	funcname RtDecoration `ri:"Attribute"`
	Name     RtToken      `ri:"name,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiAttributeBegin */
type RiAttributeBegin struct {
	funcname RtDecoration `ri:"AttributeBegin"`
	depth    RtDecoration `ri:"1"`
}

/* RiAttributeEnd */
type RiAttributeEnd struct {
	depth    RtDecoration `ri:"-1"`
	funcname RtDecoration `ri:"AttributeEnd"`
}

/* RiBasis */
type RiBasis struct {
	funcname RtDecoration `ri:"Basis"`
	U        RtBasis      `ri:"u,[16]float64"`
	Ustep    RtInt        `ri:"ustep,int"`
	V        RtBasis      `ri:"v,[16]float64"`
	Vstep    RtInt        `ri:"vstep,int"`
}

/* RiBegin */
type RiBegin struct {
	funcname RtDecoration `ri:"Begin"`
	depth    RtDecoration `ri:"1"`
	Name     RtToken      `ri:"name,string"`
}

/* RiBlobby */
type RiBlobby struct {
	funcname RtDecoration `ri:"Blobby"`
	Nleaf    RtInt        `ri:"nleaf,int"`
	Inst     []RtInt      `ri:"inst,[]int"`
	Flt      []RtFloat    `ri:"flt,[]float64"`
	Str      []RtToken    `ri:"str,[]string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiBound */
type RiBound struct {
	funcname RtDecoration `ri:"Bound"`
	Bound    RtBound      `ri:"bound,[6]float64"`
}

/* RiBxdf */
type RiBxdf struct {
	funcname RtDecoration `ri:"Bxdf"`
	Name     RtToken      `ri:"name,string"`
	Handle   RtToken      `ri:"handle,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiCamera */
type RiCamera struct {
	funcname RtDecoration `ri:"Camera"`
	Name     RtToken      `ri:"name,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiClipping */
type RiClipping struct {
	funcname  RtDecoration `ri:"Clipping"`
	Nearplane RtFloat      `ri:"nearplane,float64"`
	Farplane  RtFloat      `ri:"farplane,float64"`
}

/* RiClippingPlane */
type RiClippingPlane struct {
	funcname RtDecoration `ri:"ClippingPlane"`
	Nx       RtFloat      `ri:"Nx,float64"`
	Ny       RtFloat      `ri:"Ny,float64"`
	Nz       RtFloat      `ri:"Nz,float64"`
	Px       RtFloat      `ri:"Px,float64"`
	Py       RtFloat      `ri:"Py,float64"`
	Pz       RtFloat      `ri:"Pz,float64"`
}

/* RiColor */
type RiColor struct {
	funcname RtDecoration `ri:"Color"`
	Color    []RtFloat    `ri:"color,[]float64"`
}

/* RiColorSamples */
type RiColorSamples struct {
	funcname RtDecoration `ri:"ColorSamples"`
	NRGB     []RtFloat    `ri:"nRGB,[]float64"`
	RGBn     []RtFloat    `ri:"RGBn,[]float64"`
}

/* RiConcatTransform */
type RiConcatTransform struct {
	funcname RtDecoration `ri:"ConcatTransform"`
	M        RtMatrix     `ri:"m,[16]float64"`
}

/* RiCone */
type RiCone struct {
	funcname RtDecoration `ri:"Cone"`
	Height   RtFloat      `ri:"height,float64"`
	Radius   RtFloat      `ri:"radius,float64"`
	Tmax     RtFloat      `ri:"tmax,float64"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiCoordSysTransform */
type RiCoordSysTransform struct {
	funcname  RtDecoration `ri:"CoordSysTransform"`
	Fromspace RtToken      `ri:"fromspace,string"`
}

/* RiCoordinateSystem */
type RiCoordinateSystem struct {
	funcname RtDecoration `ri:"CoordinateSystem"`
	Name     RtString     `ri:"name,string"`
}

/* RiCropWindow */
type RiCropWindow struct {
	funcname RtDecoration `ri:"CropWindow"`
	Left     RtFloat      `ri:"left,float64"`
	Right    RtFloat      `ri:"right,float64"`
	Bottom   RtFloat      `ri:"bottom,float64"`
	Top      RtFloat      `ri:"top,float64"`
}

/* RiCurves */
type RiCurves struct {
	funcname  RtDecoration `ri:"Curves"`
	Type      RtToken      `ri:"type,string"`
	Nvertices []RtInt      `ri:"nvertices,[]int"`
	Wrap      RtToken      `ri:"wrap,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiCylinder */
type RiCylinder struct {
	funcname RtDecoration `ri:"Cylinder"`
	Radius   RtFloat      `ri:"radius,float64"`
	Zmin     RtFloat      `ri:"zmin,float64"`
	Zmax     RtFloat      `ri:"zmax,float64"`
	Tmax     RtFloat      `ri:"tmax,float64"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiDeclare */
type RiDeclare struct {
	funcname RtDecoration `ri:"Declare"`
	Name     RtString     `ri:"name,string"`
	Decl     RtString     `ri:"decl,string"`
}

/* RiDeformation */
type RiDeformation struct {
	funcname RtDecoration `ri:"Deformation"`
	Name     RtToken      `ri:"name,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiDepthOfField */
type RiDepthOfField struct {
	funcname RtDecoration `ri:"DepthOfField"`
	Fstop    RtFloat      `ri:"fstop,float64"`
	Length   RtFloat      `ri:"length,float64"`
	Distance RtFloat      `ri:"distance,float64"`
}

/* RiDetail */
type RiDetail struct {
	funcname RtDecoration `ri:"Detail"`
	Bound    RtBound      `ri:"bound,[6]float64"`
}

/* RiDetailRange */
type RiDetailRange struct {
	funcname RtDecoration `ri:"DetailRange"`
	Minvis   RtFloat      `ri:"minvis,float64"`
	Lotrans  RtFloat      `ri:"lotrans,float64"`
	Hitrans  RtFloat      `ri:"hitrans,float64"`
	Maxvis   RtFloat      `ri:"maxvis,float64"`
}

/* RiDisk */
type RiDisk struct {
	funcname RtDecoration `ri:"Disk"`
	Height   RtFloat      `ri:"height,float64"`
	Radius   RtFloat      `ri:"radius,float64"`
	Tmax     RtFloat      `ri:"tmax,float64"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiDisplace */
type RiDisplace struct {
	funcname RtDecoration `ri:"Displace"`
	Name     RtToken      `ri:"name,string"`
	Handle   RtToken      `ri:"handle,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiDisplacement */
type RiDisplacement struct {
	funcname RtDecoration `ri:"Displacement"`
	Name     RtToken      `ri:"name,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiDisplay */
type RiDisplay struct {
	funcname RtDecoration `ri:"Display"`
	Name     RtString     `ri:"name,string"`
	Type     RtToken      `ri:"type,string"`
	Mode     RtToken      `ri:"mode,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiDisplayChannel */
type RiDisplayChannel struct {
	funcname RtDecoration `ri:"DisplayChannel"`
	Channel  RtToken      `ri:"channel,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiDisplayFilter */
type RiDisplayFilter struct {
	funcname RtDecoration `ri:"DisplayFilter"`
	Name     RtToken      `ri:"name,string"`
	Handle   RtToken      `ri:"handle,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiEditAttributeBegin */
type RiEditAttributeBegin struct {
	funcname RtDecoration `ri:"EditAttributeBegin"`
	depth    RtDecoration `ri:"1"`
	Name     RtToken      `ri:"name,string"`
}

/* RiEditAttributeEnd */
type RiEditAttributeEnd struct {
	depth    RtDecoration `ri:"-1"`
	funcname RtDecoration `ri:"EditAttributeEnd"`
}

/* RiEditBegin */
type RiEditBegin struct {
	funcname RtDecoration `ri:"EditBegin"`
	depth    RtDecoration `ri:"1"`
	Name     RtToken      `ri:"name,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiEditEnd */
type RiEditEnd struct {
	depth    RtDecoration `ri:"-1"`
	funcname RtDecoration `ri:"EditEnd"`
}

/* RiEditWorldBegin */
type RiEditWorldBegin struct {
	funcname RtDecoration `ri:"EditWorldBegin"`
	depth    RtDecoration `ri:"1"`
	Name     RtToken      `ri:"name,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiEditWorldEnd */
type RiEditWorldEnd struct {
	depth    RtDecoration `ri:"-1"`
	funcname RtDecoration `ri:"EditWorldEnd"`
}

/* RiElse */
type RiElse struct {
	funcname RtDecoration `ri:"Else"`
}

/* RiElseIf */
type RiElseIf struct {
	funcname RtDecoration `ri:"ElseIf"`
	Expr     RtString     `ri:"expr,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiEnableLightFilter */
type RiEnableLightFilter struct {
	funcname RtDecoration  `ri:"EnableLightFilter"`
	Light    RtLightHandle `ri:"light,string"`
	Filter   RtToken       `ri:"filter,string"`
	Onoff    RtBoolean     `ri:"onoff,bool"`
}

/* RiEnd */
type RiEnd struct {
	depth    RtDecoration `ri:"-1"`
	funcname RtDecoration `ri:"End"`
}

/* RiExposure */
type RiExposure struct {
	funcname RtDecoration `ri:"Exposure"`
	Gain     RtFloat      `ri:"gain,float64"`
	Gamma    RtFloat      `ri:"gamma,float64"`
}

/* RiExterior */
type RiExterior struct {
	funcname RtDecoration `ri:"Exterior"`
	Name     RtToken      `ri:"name,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiFormat */
type RiFormat struct {
	funcname RtDecoration `ri:"Format"`
	Xres     RtInt        `ri:"xres,int"`
	Yres     RtInt        `ri:"yres,int"`
	Pixel_ar RtFloat      `ri:"pixel_ar,float64"`
}

/* RiFrameAspectRatio */
type RiFrameAspectRatio struct {
	funcname RtDecoration `ri:"FrameAspectRatio"`
	Ar       RtFloat      `ri:"ar,float64"`
}

/* RiFrameBegin */
type RiFrameBegin struct {
	funcname RtDecoration `ri:"FrameBegin"`
	depth    RtDecoration `ri:"1"`
	Frame    RtInt        `ri:"frame,int"`
}

/* RiFrameEnd */
type RiFrameEnd struct {
	depth    RtDecoration `ri:"-1"`
	funcname RtDecoration `ri:"FrameEnd"`
}

/* RiGeneralPolygon */
type RiGeneralPolygon struct {
	funcname RtDecoration `ri:"GeneralPolygon"`
	Nverts   []RtInt      `ri:"nverts,[]int"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiGeometricApproximation */
type RiGeometricApproximation struct {
	funcname RtDecoration `ri:"GeometricApproximation"`
	Type     RtToken      `ri:"type,string"`
	Value    RtFloat      `ri:"value,float64"`
}

/* RiGeometry */
type RiGeometry struct {
	funcname RtDecoration `ri:"Geometry"`
	Name     RtToken      `ri:"name,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiHider */
type RiHider struct {
	funcname RtDecoration `ri:"Hider"`
	Name     RtToken      `ri:"name,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiHierarchicalSubdivisionMesh */
type RiHierarchicalSubdivisionMesh struct {
	funcname   RtDecoration `ri:"HierarchicalSubdivisionMesh"`
	Mask       RtToken      `ri:"mask,string"`
	Nf         RtInt        `ri:"nf,int"`
	Nverts     []RtInt      `ri:"nverts,[]int"`
	Verts      []RtInt      `ri:"verts,[]int"`
	Nt         RtInt        `ri:"nt,int"`
	Tags       []RtToken    `ri:"tags,[]string"`
	Nargs      []RtInt      `ri:"nargs,[]int"`
	Intargs    []RtInt      `ri:"intargs,[]int"`
	Floatargs  []RtFloat    `ri:"floatargs,[]float64"`
	Stringargs []RtToken    `ri:"stringargs,[]string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiHyperboloid */
type RiHyperboloid struct {
	funcname RtDecoration `ri:"Hyperboloid"`
	Point1   RtPoint      `ri:"point1,[3]float64"`
	Point2   RtPoint      `ri:"point2,[3]float64"`
	Tmax     RtFloat      `ri:"tmax,float64"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiIdentity */
type RiIdentity struct {
	funcname RtDecoration `ri:"Identity"`
}

/* RiIfBegin */
type RiIfBegin struct {
	funcname RtDecoration `ri:"IfBegin"`
	depth    RtDecoration `ri:"1"`
	Expr     RtString     `ri:"expr,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiIfEnd */
type RiIfEnd struct {
	depth    RtDecoration `ri:"-1"`
	funcname RtDecoration `ri:"IfEnd"`
}

/* RiIlluminate */
type RiIlluminate struct {
	funcname RtDecoration  `ri:"Illuminate"`
	Light    RtLightHandle `ri:"light,string"`
	Onoff    RtBoolean     `ri:"onoff,bool"`
}

/* RiImager */
type RiImager struct {
	funcname RtDecoration `ri:"Imager"`
	Name     RtToken      `ri:"name,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiIntegrator */
type RiIntegrator struct {
	funcname RtDecoration `ri:"Integrator"`
	Name     RtToken      `ri:"name,string"`
	Handle   RtToken      `ri:"handle,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiInterior */
type RiInterior struct {
	funcname RtDecoration `ri:"Interior"`
	Name     RtToken      `ri:"name,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiLight */
type RiLight struct {
	funcname RtDecoration `ri:"Light"`
	Name     RtToken      `ri:"name,string"`
	Handle   RtToken      `ri:"handle,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiLightFilter */
type RiLightFilter struct {
	funcname RtDecoration `ri:"LightFilter"`
	Name     RtToken      `ri:"name,string"`
	Handle   RtToken      `ri:"handle,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiLightSource */
type RiLightSource struct {
	funcname RtDecoration `ri:"LightSource"`
	Name     RtToken      `ri:"name,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiMakeBrickMap */
type RiMakeBrickMap struct {
	funcname RtDecoration `ri:"MakeBrickMap"`
	Nptcs    RtInt        `ri:"nptcs,int"`
	Bkm      RtString     `ri:"bkm,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiMakeBump */
type RiMakeBump struct {
	funcname RtDecoration `ri:"MakeBump"`
	Pic      RtString     `ri:"pic,string"`
	Text     RtString     `ri:"text,string"`
	Swrap    RtToken      `ri:"swrap,string"`
	Twrap    RtToken      `ri:"twrap,string"`
	Filt     RtFilterFunc `ri:"filt,string"`
	Swidth   RtFloat      `ri:"swidth,float64"`
	Twidth   RtFloat      `ri:"twidth,float64"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiMakeCubeFaceEnvironment */
type RiMakeCubeFaceEnvironment struct {
	funcname RtDecoration `ri:"MakeCubeFaceEnvironment"`
	Px       RtString     `ri:"px,string"`
	Nx       RtString     `ri:"nx,string"`
	Py       RtString     `ri:"py,string"`
	Ny       RtString     `ri:"ny,string"`
	Pz       RtString     `ri:"pz,string"`
	Nz       RtString     `ri:"nz,string"`
	Text     RtString     `ri:"text,string"`
	Fov      RtFloat      `ri:"fov,float64"`
	Filt     RtFilterFunc `ri:"filt,string"`
	Swidth   RtFloat      `ri:"swidth,float64"`
	Twidth   RtFloat      `ri:"twidth,float64"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiMakeLatLongEnvironment */
type RiMakeLatLongEnvironment struct {
	funcname RtDecoration `ri:"MakeLatLongEnvironment"`
	Pic      RtString     `ri:"pic,string"`
	Text     RtString     `ri:"text,string"`
	Filt     RtFilterFunc `ri:"filt,string"`
	Swidth   RtFloat      `ri:"swidth,float64"`
	Twidth   RtFloat      `ri:"twidth,float64"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiMakeShadow */
type RiMakeShadow struct {
	funcname RtDecoration `ri:"MakeShadow"`
	Pic      RtString     `ri:"pic,string"`
	Text     RtString     `ri:"text,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiMakeTexture */
type RiMakeTexture struct {
	funcname RtDecoration `ri:"MakeTexture"`
	Pic      RtString     `ri:"pic,string"`
	Text     RtString     `ri:"text,string"`
	Swrap    RtToken      `ri:"swrap,string"`
	Twrap    RtToken      `ri:"twrap,string"`
	Filt     RtFilterFunc `ri:"filt,string"`
	Swidth   RtFloat      `ri:"swidth,float64"`
	Twidth   RtFloat      `ri:"twidth,float64"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiMatte */
type RiMatte struct {
	funcname RtDecoration `ri:"Matte"`
	Onoff    RtBoolean    `ri:"onoff,bool"`
}

/* RiMotionBegin */
type RiMotionBegin struct {
	funcname RtDecoration `ri:"MotionBegin"`
	depth    RtDecoration `ri:"1"`

	Points []RtFloat `ri:"points,[]float64"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiMotionEnd */
type RiMotionEnd struct {
	depth    RtDecoration `ri:"-1"`
	funcname RtDecoration `ri:"MotionEnd"`
}

/* RiNuPatch */
type RiNuPatch struct {
	funcname RtDecoration `ri:"NuPatch"`
	Nu       RtInt        `ri:"nu,int"`
	Uorder   RtInt        `ri:"uorder,int"`
	Uknot    []RtFloat    `ri:"uknot,[]float64"`
	Umin     RtFloat      `ri:"umin,float64"`
	Umax     RtFloat      `ri:"umax,float64"`
	Nv       RtInt        `ri:"nv,int"`
	Vorder   RtInt        `ri:"vorder,int"`
	Vknot    []RtFloat    `ri:"vknot,[]float64"`
	Vmin     RtFloat      `ri:"vmin,float64"`
	Vmax     RtFloat      `ri:"vmax,float64"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiObjectBegin */
type RiObjectBegin struct {
	funcname RtDecoration   `ri:"ObjectBegin"`
	depth    RtDecoration   `ri:"1"`
	Handle   RtObjectHandle `ri:"handle,string"`
}

/* RiObjectEnd */
type RiObjectEnd struct {
	depth    RtDecoration `ri:"-1"`
	funcname RtDecoration `ri:"ObjectEnd"`
}

/* RiObjectInstance */
type RiObjectInstance struct {
	funcname RtDecoration   `ri:"ObjectInstance"`
	H        RtObjectHandle `ri:"h,string"`
}

/* RiOpacity */
type RiOpacity struct {
	funcname RtDecoration `ri:"Opacity"`
	Color    []RtFloat    `ri:"color,[]float64"`
}

/* RiOption */
type RiOption struct {
	funcname RtDecoration `ri:"Option"`
	Name     RtToken      `ri:"name,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiOrientation */
type RiOrientation struct {
	funcname RtDecoration `ri:"Orientation"`
	Orient   RtToken      `ri:"orient,string"`
}

/* RiParaboloid */
type RiParaboloid struct {
	funcname RtDecoration `ri:"Paraboloid"`
	Radius   RtFloat      `ri:"radius,float64"`
	Zmin     RtFloat      `ri:"zmin,float64"`
	Zmax     RtFloat      `ri:"zmax,float64"`
	Tmax     RtFloat      `ri:"tmax,float64"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiPatch */
type RiPatch struct {
	funcname RtDecoration `ri:"Patch"`
	Type     RtToken      `ri:"type,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiPatchMesh */
type RiPatchMesh struct {
	funcname RtDecoration `ri:"PatchMesh"`
	Type     RtToken      `ri:"type,string"`
	Nu       RtInt        `ri:"nu,int"`
	Uwrap    RtToken      `ri:"uwrap,string"`
	Nv       RtInt        `ri:"nv,int"`
	Vwrap    RtToken      `ri:"vwrap,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiPattern */
type RiPattern struct {
	funcname RtDecoration `ri:"Pattern"`
	Name     RtToken      `ri:"name,string"`
	Handle   RtToken      `ri:"handle,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiPerspective */
type RiPerspective struct {
	funcname RtDecoration `ri:"Perspective"`
	Fov      RtFloat      `ri:"fov,float64"`
}

/* RiPixelFilter */
type RiPixelFilter struct {
	funcname RtDecoration `ri:"PixelFilter"`
	Func     RtFilterFunc `ri:"func,string"`
	Xwidth   RtFloat      `ri:"xwidth,float64"`
	Ywidth   RtFloat      `ri:"ywidth,float64"`
}

/* RiPixelSampleImager */
type RiPixelSampleImager struct {
	funcname RtDecoration `ri:"PixelSampleImager"`
	Name     RtToken      `ri:"name,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiPixelSamples */
type RiPixelSamples struct {
	funcname RtDecoration `ri:"PixelSamples"`
	X        RtFloat      `ri:"x,float64"`
	Y        RtFloat      `ri:"y,float64"`
}

/* RiPixelVariance */
type RiPixelVariance struct {
	funcname RtDecoration `ri:"PixelVariance"`
	Var      RtFloat      `ri:"var,float64"`
}

/* RiPoints */
type RiPoints struct {
	funcname RtDecoration `ri:"Points"`
	Nverts   RtInt        `ri:"nverts,int"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiPointsGeneralPolygons */
type RiPointsGeneralPolygons struct {
	funcname RtDecoration `ri:"PointsGeneralPolygons"`
	Npolys   RtInt        `ri:"npolys,int"`
	Nloops   []RtInt      `ri:"nloops,[]int"`
	Nverts   []RtInt      `ri:"nverts,[]int"`
	Verts    []RtInt      `ri:"verts,[]int"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiPointsPolygons */
type RiPointsPolygons struct {
	funcname RtDecoration `ri:"PointsPolygons"`
	Npolys   RtInt        `ri:"npolys,int"`
	Nverts   []RtInt      `ri:"nverts,[]int"`
	Verts    []RtInt      `ri:"verts,[]int"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiPolygon */
type RiPolygon struct {
	funcname RtDecoration `ri:"Polygon"`
	Nverts   RtInt        `ri:"nverts,int"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiProcedural */
type RiProcedural struct {
	funcname   RtDecoration     `ri:"Procedural"`
	SubdivFunc RtProcSubdivFunc `ri:"sfunc,string"`
	Data       []RtString       `ri:"data,[]string"`
	Bound      RtBound          `ri:"bound,[6]float64"`

	//	FreeFunc		 RtProcFreeFunc		`ri:"ffunc,string"`
}

/* RiProcedural2 */
type RiProcedural2 struct {
	funcname RtDecoration `ri:"Procedural2"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiProjection */
type RiProjection struct {
	funcname RtDecoration `ri:"Projection"`
	Name     RtToken      `ri:"name,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiQuantize */
type RiQuantize struct {
	funcname RtDecoration `ri:"Quantize"`
	Type     RtToken      `ri:"type,string"`
	One      RtInt        `ri:"one,int"`
	Min      RtInt        `ri:"min,int"`
	Max      RtInt        `ri:"max,int"`
	Dither   RtFloat      `ri:"dither,float64"`
}

/* RiReadArchive */
type RiReadArchive struct {
	funcname RtDecoration      `ri:"ReadArchive"`
	Name     RtToken           `ri:"name,string"`
	Callback RtArchiveCallback `ri:"-"` /* not used in the parser */

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiRelativeDetail */
type RiRelativeDetail struct {
	funcname RtDecoration `ri:"RelativeDetail"`
	Rel      RtFloat      `ri:"rel,float64"`
}

/* RiResource */
type RiResource struct {
	funcname RtDecoration `ri:"Resource"`
	Handle   RtToken      `ri:"handle,string"`
	Type     RtToken      `ri:"type,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiResourceBegin */
type RiResourceBegin struct {
	funcname RtDecoration `ri:"ResourceBegin"`
	depth    RtDecoration `ri:"1"`
}

/* RiResourceEnd */
type RiResourceEnd struct {
	depth    RtDecoration `ri:"-1"`
	funcname RtDecoration `ri:"ResourceEnd"`
}

/* RiReverseOrientation */
type RiReverseOrientation struct {
	funcname RtDecoration `ri:"ReverseOrientation"`
}

/* RiRotate */
type RiRotate struct {
	funcname RtDecoration `ri:"Rotate"`
	Angle    RtFloat      `ri:"angle,float64"`
	Dx       RtFloat      `ri:"dx,float64"`
	Dy       RtFloat      `ri:"dy,float64"`
	Dz       RtFloat      `ri:"dz,float64"`
}

/* RiSampleFilter */
type RiSampleFilter struct {
	funcname RtDecoration `ri:"SampleFilter"`
	Name     RtToken      `ri:"name,string"`
	Handle   RtToken      `ri:"handle,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiScale */
type RiScale struct {
	funcname RtDecoration `ri:"Scale"`
	Sx       RtFloat      `ri:"sx,float64"`
	Sy       RtFloat      `ri:"sy,float64"`
	Sz       RtFloat      `ri:"sz,float64"`
}

/* RiScopedCoordinateSystem */
type RiScopedCoordinateSystem struct {
	funcname RtDecoration `ri:"ScopedCoordinateSystem"`
	Name     RtString     `ri:"name,string"`
}

/* RiScreenWindow */
type RiScreenWindow struct {
	funcname RtDecoration `ri:"ScreenWindow"`
	Left     RtFloat      `ri:"left,float64"`
	Right    RtFloat      `ri:"right,float64"`
	Bottom   RtFloat      `ri:"bottom,float64"`
	Top      RtFloat      `ri:"top,float64"`
}

/* RiShader */
type RiShader struct {
	funcname RtDecoration `ri:"Shader"`
	Name     RtToken      `ri:"name,string"`
	Handle   RtToken      `ri:"handle,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiShadingInterpolation */
type RiShadingInterpolation struct {
	funcname RtDecoration `ri:"ShadingInterpolation"`
	Type     RtToken      `ri:"type,string"`
}

/* RiShadingRate */
type RiShadingRate struct {
	funcname RtDecoration `ri:"ShadingRate"`
	Size     RtFloat      `ri:"size,float64"`
}

/* RiShutter */
type RiShutter struct {
	funcname  RtDecoration `ri:"Shutter"`
	Opentime  RtFloat      `ri:"opentime,float64"`
	Closetime RtFloat      `ri:"closetime,float64"`
}

/* RiSides */
type RiSides struct {
	funcname RtDecoration `ri:"Sides"`
	N        RtInt        `ri:"n,int"`
}

/* RiSkew */
type RiSkew struct {
	funcname RtDecoration `ri:"Skew"`
	Angle    RtFloat      `ri:"angle,float64"`
	D1x      RtFloat      `ri:"d1x,float64"`
	D1y      RtFloat      `ri:"d1y,float64"`
	D1z      RtFloat      `ri:"d1z,float64"`
	D2x      RtFloat      `ri:"d2x,float64"`
	D2y      RtFloat      `ri:"d2y,float64"`
	D2z      RtFloat      `ri:"d2z,float64"`
}

/* RiSolidBegin */
type RiSolidBegin struct {
	funcname RtDecoration `ri:"SolidBegin"`
	depth    RtDecoration `ri:"1"`
	Op       RtString     `ri:"op,string"`
}

/* RiSolidEnd */
type RiSolidEnd struct {
	depth    RtDecoration `ri:"-1"`
	funcname RtDecoration `ri:"SolidEnd"`
}

/* RiSphere */
type RiSphere struct {
	funcname RtDecoration `ri:"Sphere"`
	Radius   RtFloat      `ri:"radius,float64"`
	Zmin     RtFloat      `ri:"zmin,float64"`
	Zmax     RtFloat      `ri:"zmax,float64"`
	Tmax     RtFloat      `ri:"tmax,float64"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiSubdivisionMesh */
type RiSubdivisionMesh struct {
	funcname  RtDecoration `ri:"SubdivisionMesh"`
	Mask      RtToken      `ri:"mask,string"`
	Nf        RtInt        `ri:"nf,int"`
	Nverts    []RtInt      `ri:"nverts,[]int"`
	Verts     []RtInt      `ri:"verts,[]int"`
	Nt        RtInt        `ri:"nt,int"`
	Tags      []RtToken    `ri:"tags,[]string"`
	Nargs     []RtInt      `ri:"nargs,[]int"`
	Intargs   []RtInt      `ri:"intargs,[]int"`
	Floatargs []RtFloat    `ri:"floatargs,[]float64"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiSurface */
type RiSurface struct {
	funcname RtDecoration `ri:"Surface"`
	Name     RtToken      `ri:"name,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiSystem */
type RiSystem struct {
	funcname RtDecoration `ri:"System"`
	Name     RtString     `ri:"name,string"`
}

/* RiTextureCoordinates */
type RiTextureCoordinates struct {
	funcname RtDecoration `ri:"TextureCoordinates"`
	S1       RtFloat      `ri:"s1,float64"`
	T1       RtFloat      `ri:"t1,float64"`
	S2       RtFloat      `ri:"s2,float64"`
	T2       RtFloat      `ri:"t2,float64"`
	S3       RtFloat      `ri:"s3,float64"`
	T3       RtFloat      `ri:"t3,float64"`
	S4       RtFloat      `ri:"s4,float64"`
	T4       RtFloat      `ri:"t4,float64"`
}

/* RiTorus */
type RiTorus struct {
	funcname RtDecoration `ri:"Torus"`
	Majrad   RtFloat      `ri:"majrad,float64"`
	Minrad   RtFloat      `ri:"minrad,float64"`
	Phimin   RtFloat      `ri:"phimin,float64"`
	Phimax   RtFloat      `ri:"phimax,float64"`
	Tmax     RtFloat      `ri:"tmax,float64"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiTransform */
type RiTransform struct {
	funcname RtDecoration `ri:"Transform"`
	M        RtMatrix     `ri:"m,[16]float64"`
}

/* RiTransformBegin */
type RiTransformBegin struct {
	funcname RtDecoration `ri:"TransformBegin"`
	depth    RtDecoration `ri:"1"`
}

/* RiTransformEnd */
type RiTransformEnd struct {
	depth    RtDecoration `ri:"-1"`
	funcname RtDecoration `ri:"TransformEnd"`
}

/* RiTransformPoints */
type RiTransformPoints struct {
	funcname  RtDecoration `ri:"TransformPoints"`
	Fromspace RtToken      `ri:"fromspace,string"`
	Tospace   RtToken      `ri:"tospace,string"`
	N         RtInt        `ri:"n,int"`
	Points    []RtPoint    `ri:"points,[][3]float64"`
}

/* RiTranslate */
type RiTranslate struct {
	funcname RtDecoration `ri:"Translate"`
	Dx       RtFloat      `ri:"dx,float64"`
	Dy       RtFloat      `ri:"dy,float64"`
	Dz       RtFloat      `ri:"dz,float64"`
}

/* RiTrimCurve */
type RiTrimCurve struct {
	funcname RtDecoration `ri:"TrimCurve"`
	Nloops   RtInt        `ri:"nloops,int"`
	Ncurves  []RtInt      `ri:"ncurves,[]int"`
	Order    []RtInt      `ri:"order,[]int"`
	Knot     []RtFloat    `ri:"knot,[]float64"`
	Min      []RtFloat    `ri:"min,[]float64"`
	Max      []RtFloat    `ri:"max,[]float64"`
	N        []RtInt      `ri:"n,[]int"`
	U        []RtFloat    `ri:"u,[]float64"`
	V        []RtFloat    `ri:"v,[]float64"`
	W        []RtFloat    `ri:"w,[]float64"`
}

/* RiVArchiveRecord */
type RiVArchiveRecord struct {
	funcname RtDecoration `ri:"VArchiveRecord"`
	Type     RtToken      `ri:"type,string"`
}

/* RiVPAtmosphere */
type RiVPAtmosphere struct {
	funcname RtDecoration `ri:"VPAtmosphere"`
	Name     RtToken      `ri:"name,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiVPInterior */
type RiVPInterior struct {
	funcname RtDecoration `ri:"VPInterior"`
	Name     RtToken      `ri:"name,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiVPSurface */
type RiVPSurface struct {
	funcname RtDecoration `ri:"VPSurface"`
	Name     RtToken      `ri:"name,string"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiVolume */
type RiVolume struct {
	funcname   RtDecoration `ri:"Volume"`
	Type       RtToken      `ri:"type,string"`
	Bound      RtBound      `ri:"bound,[6]float64"`
	Dimensions []RtInt      `ri:"dimensions,[]int"`

	params RtDecoration `ri:"parameterlist"`
	Params RtParameters `ri:"params"`
}

/* RiVolumePixelSamples */
type RiVolumePixelSamples struct {
	funcname RtDecoration `ri:"VolumePixelSamples"`
	X        RtFloat      `ri:"x,float64"`
	Y        RtFloat      `ri:"y,float64"`
}

/* RiWorldBegin */
type RiWorldBegin struct {
	funcname RtDecoration `ri:"WorldBegin"`
	depth    RtDecoration `ri:"1"`
}

/* RiWorldEnd */
type RiWorldEnd struct {
	depth    RtDecoration `ri:"-1"`
	funcname RtDecoration `ri:"WorldEnd"`
}

var All = []interface{}{RiArchiveBegin{}, RiArchiveEnd{}, RiArchiveRecord{}, RiAreaLightSource{}, RiAtmosphere{}, RiAttribute{}, RiAttributeBegin{}, RiAttributeEnd{}, RiBasis{}, RiBegin{}, RiBlobby{}, RiBound{}, RiBxdf{}, RiCamera{}, RiClipping{}, RiClippingPlane{}, RiColor{}, RiColorSamples{}, RiConcatTransform{}, RiCone{}, RiCoordSysTransform{}, RiCoordinateSystem{}, RiCropWindow{}, RiCurves{}, RiCylinder{}, RiDeclare{}, RiDeformation{}, RiDepthOfField{}, RiDetail{}, RiDetailRange{}, RiDisk{}, RiDisplace{}, RiDisplacement{}, RiDisplay{}, RiDisplayChannel{}, RiDisplayFilter{}, RiEditAttributeBegin{}, RiEditAttributeEnd{}, RiEditBegin{}, RiEditEnd{}, RiEditWorldBegin{}, RiEditWorldEnd{}, RiElse{}, RiElseIf{}, RiEnableLightFilter{}, RiEnd{}, RiExposure{}, RiExterior{}, RiFormat{}, RiFrameAspectRatio{}, RiFrameBegin{}, RiFrameEnd{}, RiGeneralPolygon{}, RiGeometricApproximation{}, RiGeometry{}, RiHider{}, RiHierarchicalSubdivisionMesh{}, RiHyperboloid{}, RiIdentity{}, RiIfBegin{}, RiIfEnd{}, RiIlluminate{}, RiImager{}, RiIntegrator{}, RiInterior{}, RiLight{}, RiLightFilter{}, RiLightSource{}, RiMakeBrickMap{}, RiMakeBump{}, RiMakeCubeFaceEnvironment{}, RiMakeLatLongEnvironment{}, RiMakeShadow{}, RiMakeTexture{}, RiMatte{}, RiMotionBegin{}, RiMotionEnd{}, RiNuPatch{}, RiObjectBegin{}, RiObjectEnd{}, RiObjectInstance{}, RiOpacity{}, RiOption{}, RiOrientation{}, RiParaboloid{}, RiPatch{}, RiPatchMesh{}, RiPattern{}, RiPerspective{}, RiPixelFilter{}, RiPixelSampleImager{}, RiPixelSamples{}, RiPixelVariance{}, RiPoints{}, RiPointsGeneralPolygons{}, RiPointsPolygons{}, RiPolygon{}, RiProcedural{}, RiProcedural2{}, RiProjection{}, RiQuantize{}, RiReadArchive{}, RiRelativeDetail{}, RiResource{}, RiResourceBegin{}, RiResourceEnd{}, RiReverseOrientation{}, RiRotate{}, RiSampleFilter{}, RiScale{}, RiScopedCoordinateSystem{}, RiScreenWindow{}, RiShader{}, RiShadingInterpolation{}, RiShadingRate{}, RiShutter{}, RiSides{}, RiSkew{}, RiSolidBegin{}, RiSolidEnd{}, RiSphere{}, RiSubdivisionMesh{}, RiSurface{}, RiSystem{}, RiTextureCoordinates{}, RiTorus{}, RiTransform{}, RiTransformBegin{}, RiTransformEnd{}, RiTransformPoints{}, RiTranslate{}, RiTrimCurve{}, RiVArchiveRecord{}, RiVPAtmosphere{}, RiVPInterior{}, RiVPSurface{}, RiVolume{}, RiVolumePixelSamples{}, RiWorldBegin{}, RiWorldEnd{}}

/* EOF */
