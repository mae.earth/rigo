package rib

/* TODO:
 * the ri prototypes, as machine generated from the header files is incorrect,
 * as the RIB version of the commands are generally not the exact same.
 *
 * Solution, hand annotate the functions for RIB entry, testing against the output
 * from prman. Then adjust the machine generation to take into account these annotations.
 */

type Tokeniser struct {
	buffer []*token
}

func NewTokeniser() *Tokeniser {

	t := &Tokeniser{}
	t.buffer = make([]*token, 0)
	return t
}

/* continue_tokeniser */
func continue_tokeniser(tokeniser *Tokeniser, stream []*token) ([]*token_composite, error) {

	tokeniser.buffer = append(tokeniser.buffer, stream...)
	token_stream := make([]*token_composite, 0)

	for _, tkn := range tokeniser.buffer {

		switch tkn.Type {
		case TokenArchive:

			if len(token_stream) > 0 {
				last := token_stream[len(token_stream)-1]
				if len(last.Tokens) > 0 && last.Tokens[0].Type == TokenLiteral {

					last.Tokens = append(last.Tokens, tkn)
					continue
				}

				if len(last.Tokens) == 0 {
					last.Tokens = append(last.Tokens, tkn)
					continue
				}
			}

			tc := &token_composite{}
			tc.Tokens = make([]*token, 0)
			tc.Tokens = append(tc.Tokens, tkn)

			token_stream = append(token_stream, tc)

			break
		case TokenLiteral:

			/* check if the current tc starts a literal, if so then this is the end */
			if len(token_stream) > 0 {

				last := token_stream[len(token_stream)-1]

				if len(last.Tokens) > 0 {
					if last.Tokens[0].Type == TokenLiteral {

						/* then finish this tc off and start a new */
						token_stream[len(token_stream)-1].Tokens = append(token_stream[len(token_stream)-1].Tokens, tkn)
						tc := &token_composite{}
						tc.Tokens = make([]*token, 0)

						token_stream = append(token_stream, tc)
						continue
					}
					if last.Tokens[0].Type == TokenOpen {
						last.Tokens = append(last.Tokens, tkn)
						continue
					}
				}

				if len(last.Tokens) == 0 {
					last.Tokens = append(last.Tokens, tkn)
					continue
				}

			}

			tc := &token_composite{}
			tc.Tokens = make([]*token, 0)
			tc.Tokens = append(tc.Tokens, tkn)

			token_stream = append(token_stream, tc)

			break
		case TokenOpen:

			if len(token_stream) > 0 {
				last := token_stream[len(token_stream)-1]
				if len(last.Tokens) > 0 && last.Tokens[0].Type == TokenLiteral {

					last.Tokens = append(last.Tokens, tkn)
					continue
				}

				if len(last.Tokens) == 0 {
					last.Tokens = append(last.Tokens, tkn)
					continue
				}
			}

			tc := &token_composite{}
			tc.Tokens = make([]*token, 0)
			tc.Tokens = append(tc.Tokens, tkn)

			token_stream = append(token_stream, tc)

			break
		case TokenClose:

			/* check if the current tc starts a literal, if so then this is the end */
			if len(token_stream) > 0 {

				last := token_stream[len(token_stream)-1]

				if len(last.Tokens) > 0 {

					if last.Tokens[0].Type == TokenOpen {

						/* then finish this tc off and start a new */
						last.Tokens = append(last.Tokens, tkn)
						tc := &token_composite{}
						tc.Tokens = make([]*token, 0)

						token_stream = append(token_stream, tc)
						continue
					} else {

						last.Tokens = append(last.Tokens, tkn)
					}
				}
			}

			break
		case TokenWord, TokenLastWord:

			if len(token_stream) > 0 {

				last := token_stream[len(token_stream)-1]

				if len(last.Tokens) > 0 {

					if last.Tokens[0].Type == TokenOpen || last.Tokens[0].Type == TokenLiteral {

						last.Tokens = append(last.Tokens, tkn)
						continue
					}
				}

				if len(last.Tokens) == 0 {

					last.Tokens = append(last.Tokens, tkn)
					continue
				}
			}

			tc := &token_composite{}
			tc.Tokens = make([]*token, 0)
			tc.Tokens = append(tc.Tokens, tkn)

			token_stream = append(token_stream, tc)

			break
		case TokenCommand:

			if len(token_stream) > 0 {
				last := token_stream[len(token_stream)-1]
				if len(last.Tokens) == 0 {
					last.Tokens = append(last.Tokens, tkn)
					continue
				}
			}

			tc := &token_composite{}
			tc.Tokens = make([]*token, 0)
			tc.Tokens = append(tc.Tokens, tkn)

			token_stream = append(token_stream, tc)
			break
		}
	}

	tokeniser.buffer = make([]*token, 0)

	return token_stream, nil
}
