package rigo

import (
	"bytes"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"os"

	mrand "math/rand"

	"gitlab.com/mae.earth/rigo/ri"

	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func Test_DirectRi(t *testing.T) {

	Convey("testing direct Ri interface", t, func() {

		f, err := os.Create("basic.rib")
		So(err, ShouldBeNil)
		defer f.Close()

		ri.Begin(f)
		ri.ArchiveRecord(ri.STRUCTURE, "%s", ri.RENDERMAN)
		ri.Display("basic.exr", "openexr", "rgba")
		ri.Format(640, 480, 1.0)
		ri.Projection("perspective", "float fov", 30.0)
		ri.Hider("raytrace")
		ri.Integrator("PxrPathTracer", "example")
		ri.WorldBegin()
		ri.Translate(0, 0, 10)
		ri.AttributeBegin()
		ri.Translate(5, 5, 5)
		light := ri.Light("PxrEnvDayLight", "-")
		So(light, ShouldNotBeEmpty)
		ri.Geometry("envsphere", "constant float[2] resolution", [2]float64{1024, 1024})
		ri.AttributeEnd()
		ri.Illuminate(light, true)
		ri.AttributeBegin()
		ri.Bxdf("PxrDisney", "surface", "color baseColor", []float64{0, 0, 1})
		ri.Sphere(1, -1, 1, 360)
		ri.AttributeEnd()
		ri.WorldEnd()

		ri.End()
	})
}

func Test_Disks(t *testing.T) {

	Convey("Disk", t, func() {

		ri.Begin("disks.rib")
		ri.ArchiveRecord(ri.STRUCTURE, "%s RIB-Structure %.2f", ri.RENDERMAN, 1.1)
		ri.Display("disks.exr", "openexr", "rgba")
		ri.Format(640, 480, 1.0)
		ri.Projection("perspective", "float fov", 30.0)
		ri.Hider("raytrace", "int maxsamples", 16, "int minsamples", 16, "int incremental", 10, "float[4] aperture", [4]float64{0, 0, 0, 0})
		ri.Integrator("PxrPathTracer", "example")
		ri.WorldBegin()
		ri.Translate(0, 0, 20)
		ri.AttributeBegin()
		ri.Translate(5, 5, 5)
		light := ri.Light("PxrEnvDayLight", "-", "float intensity", 2.0, "float exposure", 1.0)
		ri.Geometry("envsphere", "constant float[2] resolution", [2]float64{1024, 1024})
		ri.AttributeEnd()
		ri.Illuminate(light, true)
		ri.AttributeBegin()
		ri.Translate(0, 0, 0)
		light2 := ri.Light("PxrEnvDayLight", "-", "float intensity", 2.0, "float exposure", 1.0)
		ri.Geometry("envsphere", "constant float[2] resolution", [2]float64{1024, 1024})
		ri.AttributeEnd()
		ri.Illuminate(light2, true)

		s := []float64{}
		x := -10.0
		for {
			if x >= 10.0 {
				break
			}

			s = append(s, x)
			x += 1.0
		}

		for _, x := range s {
			for _, y := range s {
				ri.AttributeBegin()
				ri.Translate(x, y, 0)
				ri.Rotate(160, 1, 0, 0)
				ri.Bxdf("PxrDisney", "surface", "color baseColor", []float64{mrand.Float64(), mrand.Float64(), mrand.Float64()})

				ri.Scale(mrand.Float64(), mrand.Float64(), mrand.Float64())
				ri.Disk(1, 1, 360)
				//ri.Sphere(1,-1,1,360)
				ri.AttributeEnd()
			}
		}
		ri.WorldEnd()

		ri.End()
	})
}

func Test_Ri(t *testing.T) {

	Convey("testing Ri interface", t, func() {

		ctx := New()
		So(ctx, ShouldNotBeNil)

		f, err := os.Create("test.rib")
		So(err, ShouldBeNil)
		defer f.Close()

		callback := func(typeof, format string, params ...interface{}) {

			switch typeof {
			case ri.COMMENT:
				fmt.Fprintf(os.Stderr, "comment called %s\n", fmt.Sprintf(format, params...))
				break
			case ri.STRUCTURE:
				fmt.Fprintf(os.Stderr, "structure called %s\n", fmt.Sprintf(format, params...))
				break
			case ri.VERBATIM:
				fmt.Fprintf(os.Stderr, "verbatim called %s\n", fmt.Sprintf(format, params...))
				break
			}
		}

		errorhandler := func(err Error) {

			fmt.Fprintf(os.Stderr, "[error] %s\n", err)
			panic(err)
		}

		ctx.Begin(f)

		ctx.ErrorHandler(ErrorHandlerFunc(errorhandler))
		/* TODO: this is durality when writing out it should also	write to the RIB stream */

		ctx.ArchiveRecord(ri.STRUCTURE, "%s RIB-Structure %.1f", ri.RENDERMAN, 1.1)

		resolution := ctx.Declare("resolution", "constant float[2]")
		ctx.Declare("baseColor", "color")

		/* inline rib archive */
		archive1 := ctx.ArchiveBegin("lighting#1", "string definition", "persistent")
		ctx.AttributeBegin()
		ctx.ArchiveRecord(ri.COMMENT, "%s", "this is our lights")

		/* archive within archive test */
		ctx.ArchiveBegin("inner", "string definition", "persistent")
		ctx.Translate(5, 5, 5)
		ctx.Light("PxrEnvDayLight", "light1")
		ctx.Geometry("envsphere", resolution, [2]float64{1024, 1024})
		ctx.ArchiveEnd()

		ctx.ReadArchive("inner", nil)

		ctx.AttributeEnd()
		ctx.ArchiveEnd()

		archive2 := ctx.ArchiveBegin("lighting#2")
		ctx.ArchiveRecord(ri.COMMENT, "%s", "this is our second light")
		ctx.AttributeBegin()
		ctx.Translate(-5, 5, -5)
		ctx.Light("PxrEnvDayLight", "light2")
		ctx.Geometry("envsphere", resolution, [2]float64{1024, 1024})
		ctx.AttributeEnd()
		ctx.ArchiveEnd()

		ctx.Display("test.exr", "openexr", "rgba")
		ctx.Format(320*2, 240*2, 1.0)

		ctx.Projection("perspective", "float fov", 30.0)
		ctx.Hider("raytrace", "int maxsamples", 256, "int minsamples", 16, "int incremental", 100, "float[4] aperture", [4]float64{0, 0, 0, 0})
		ctx.Integrator("PxrPathTracer", "production")

		ctx.Shutter(0, 1)

		ctx.System("echo -sync -jobid=^{Option:user:jobid} -renderpass=^{Option:user:renderpass} -point=WorldBegin")

		ctx.WorldBegin()

		object := ctx.ObjectBegin() /* Object Begin/End are old, should really use Archive Begin/End instead */
		ctx.Sphere(1, -1, 1, 360)
		ctx.ObjectEnd()

		ctx.ReadArchive("test2.rib", nil)
		ctx.Translate(0, 0, 10)
		ctx.ReadArchive(archive1, callback)
		ctx.Illuminate("light1", true)

		ctx.ReadArchive(archive2, callback)
		ctx.Illuminate("light2", true)
		ctx.AttributeBegin()
		ctx.MotionBegin(0.0, 0.5, 1.0)
		ctx.Translate(1, 0, 0)
		ctx.Translate(0, 0, 0)
		ctx.Translate(-1, 0, 0)
		ctx.MotionEnd()
		ctx.Bxdf("PxrDisney", "surface", "baseColor", []float64{1, 0, 0})
		ctx.ObjectInstance(object)
		ctx.AttributeEnd()

		ctx.AttributeBegin()
		ctx.MotionBegin(0.0, 0.25, 0.5, 0.75, 1.0)
		ctx.Translate(0, 1, 0)
		ctx.Translate(0, 0.5, 0)
		ctx.Translate(0, 0, 0)
		ctx.Translate(0, -0.5, 0)
		ctx.Translate(0, -1, 0)
		ctx.MotionEnd()
		ctx.Bxdf("PxrDisney", "surface", "baseColor", []float64{0, 0, 1})
		ctx.ObjectInstance(object)
		ctx.AttributeEnd()

		ctx.ReadArchive("example.rib", callback)

		/*
			ctx.AttributeBegin()
				ctx.Curves("cubic",4,[]int{4},"nonperiodic","P",[]float64{0,0,0,-1,-.5,1,2,.5,1,1,0,-1}, "width", [2]float64{.1,.04})
			ctx.AttributeEnd()
		*/

		ctx.WorldEnd()

		ctx.System("echo -sync -jobid=^{Option:user:jobid} -renderpass=^{Option:user:renderpass} -point=WorldEnd")

		for {
			if ctx.RicGetProgress() == 100 {
				break
			}
		}
		ctx.End()

		ctx = New()

		ctx.Begin("test2.rib")
		ctx.ArchiveRecord(ri.STRUCTURE, "%s RIB-Structure %.1f", ri.RENDERMAN, 1.1)

		ctx.ReadArchive(archive1, nil)
		ctx.End()

		ctx = New()

		b := make([]byte, 4)
		if _, err := rand.Read(b); err != nil {

			fmt.Fprintf(os.Stderr, "! unable to create id -- %v\n", err)
			os.Exit(1)
		}

		hash := hex.EncodeToString(b)

		ctx.Begin("driver.rib")
		ctx.ArchiveRecord(ri.STRUCTURE, "%s RIB-Structure %.1f", ri.RENDERMAN, 1.1)
		ctx.Option("ribparse", "string varsubst", "^") /* NOTE: change from '$' to '^' because the shell will consume otherwise, TODO: ensure this is used in any ReadAchive calls to the internal RIB parser. */
		ctx.Option("user", "string jobid", hash, "string renderpass", "beauty")
		ctx.End()

		/* NOTE: build with :-
			 * # go test
		   * # cat driver2.rib test2.rib > out.rib
			 * # render -d it out.rib
		*/
	})
}

func Benchmark_Basic(b *testing.B) {

	for i := 0; i < b.N; i++ {

		ctx := New()

		buf := bytes.NewBuffer(nil)

		ctx.Begin(buf)
		ctx.ArchiveRecord(ri.STRUCTURE, "%s RIB-Structure %.1f", ri.RENDERMAN, 1.1)
		ctx.Display("test.exr", "openexr", "rgba")
		ctx.Format(320*2, 240*2, 1.0)

		ctx.Projection("perspective", "float fov", 30.0)
		ctx.Hider("raytrace", "int maxsamples", 256, "int minsamples", 16, "int incremental", 100, "float[4] aperture", [4]float64{0, 0, 0, 0})
		ctx.Integrator("PxrPathTracer", "production")

		ctx.WorldBegin()
		ctx.Translate(0, 0, 10.0)
		ctx.Sphere(1, -1, 1, 360)
		ctx.WorldEnd()

		ctx.End()
	}
}
