package ri

import (
	"fmt"
	"io"
	"os"
)

type RtBoolean bool

func (o RtBoolean) String() string {
	if o {
		return "1"
	}
	return "0"
}

type RtFloat float64
type RtInt int
type RtColor []float64
type RtPoint [3]float64
type RtVector [3]float64
type RtNormal [3]float64
type RtHpoint [4]float64
type RtMatrix [16]float64
type RtBasis [16]float64
type RtBound [6]float64
type RtInterval [2]float64

type RtToken string

func (o RtToken) String() string {
	return fmt.Sprintf("%q", string(o))
}

type RtUnescapedString string

func (o RtUnescapedString) String() string {
	return string(o)
}

type RtString string

func (o RtString) String() string {
	return fmt.Sprintf("%q", string(o))
}

type RtArchiveHandle string
type RtBxdfHandle string
type RtDisplacementHandle string
type RtDisplayFilterHandle string
type RtLightHandle string

func (o RtLightHandle) String() string {
	return fmt.Sprintf("%q", string(o))
}

type RtIntegratorHandle string

func (o RtIntegratorHandle) String() string {
	return fmt.Sprintf("%q", string(o))
}

type RtObjectHandle string

func (o RtObjectHandle) String() string {
	return fmt.Sprintf("%q", string(o))
}

type RtPatternHandle string
type RtSampleFilterHandle string
type RtShaderHandle string
type RtFilterFunc string
type RtProcSubdivFunc string

func (o RtProcSubdivFunc) String() string {
	return fmt.Sprintf("%q", string(o))
}

type RtProcFreeFunc string

type RtArchiveCallback func(string, string, ...interface{})

type RtParameter struct {
	Token RtToken
	Value interface{}
}

type RtParameters []RtParameter
type RtDecoration string
type RtFuncName string
type RtAnnotation string
type RtDepth int

type Error struct {
	Code     int
	Severity int
	Msg      string
}

func (err *Error) Error() string {
	return fmt.Sprintf("%03d:%05d %s", err.Severity, err.Code, err.Msg)
}

type Driver interface {
	io.Writer
	Close() error
}

type DriverFunc func(...interface{}) Driver

type DriverWriter struct {
	io.Writer
}

func (w *DriverWriter) Close() error {
	return nil
}

type ErrorHandlerFunc func(int, int, string)
type ArchiveCallbackFunc func(string, string, ...interface{})

func ErrorPrint(code, severity int, msg string) {

	fmt.Fprintf(os.Stderr, "%03d:%05d %s", severity, code, msg)
}

func ErrorAbort(code, severity int, msg string) {

	panic(fmt.Sprintf("%03d:%05d %s", severity, code, msg))
}

type NameGenerationHandlerFunc func(name, typeof string) (string, error)

/* TODO: this can be refactored into a more elegent setup */
type RtMicroContextHandle struct {
	name       string        /* this is the user supplied name */
	handle     string        /* this is the handle generated */
	persistent bool          /* usually false, as in transient */
	operations []interface{} /* all the operations that are added between Begin && End */
}

type RtContextHandle struct {
	declarations map[string]string
	micro_lookup map[string]*RtMicroContextHandle /* gen_handle = context, used with ReadArchive(...) */
	micro_stack  []*RtMicroContextHandle          /* determines which context the user is writing to, zero on the stack means write to driver */

	wrote                int /* number of calls made */
	depth                int
	driver               Driver
	within_parameterlist bool
	parameterlist_args   int

	callback RtArchiveCallback

	error_handler func(code, severity int, msg string)
}

func (ctx *RtContextHandle) SetErrorHandler(handler func(code, severity int, msg string)) {
	ctx.error_handler = handler
}

func (ctx *RtContextHandle) Handle(riber interface{}) *Error {
	return process(ctx, riber)
}

func New() *RtContextHandle {

	ctx := new(RtContextHandle)
	ctx.declarations = make(map[string]string, 0)
	ctx.micro_lookup = make(map[string]*RtMicroContextHandle, 0)
	ctx.micro_stack = make([]*RtMicroContextHandle, 0)
	ctx.callback = nil
	return ctx
}

func NewRiWrapper() *RiWrapper {

	wrapper := new(RiWrapper)
	wrapper.ctx = new(RtContextHandle)
	wrapper.ctx.declarations = make(map[string]string, 0)
	wrapper.ctx.micro_lookup = make(map[string]*RtMicroContextHandle, 0)
	wrapper.ctx.micro_stack = make([]*RtMicroContextHandle, 0)
	wrapper.ctx.callback = nil

	/* set the some defaults */
	wrapper.ctx.declarations["name"] = "string name"

	return wrapper
}

func toRtIntArray(a []int) []RtInt {
	o := []RtInt{}
	for _, i := range a {
		o = append(o, RtInt(i))
	}
	return o
}

func toRtFloatArray(a []float64) []RtFloat {
	o := []RtFloat{}
	for _, f := range a {
		o = append(o, RtFloat(f))
	}
	return o
}

func toRtStringArray(a []string) []RtString {
	o := []RtString{}
	for _, s := range a {
		o = append(o, RtString(s))
	}
	return o
}

func toRtTokenArray(a []string) []RtToken {
	o := []RtToken{}
	for _, s := range a {
		o = append(o, RtToken(s))
	}
	return o
}

func toRtPointArray(a [][3]float64) []RtPoint {
	o := []RtPoint{}
	for _, p := range a {
		o = append(o, RtPoint{p[0], p[1], p[2]})
	}
	return o
}
