package ri

import (
	"fmt"
	"io"
	"os"
	"reflect"
	"strconv"
	"sync"
)

var internal struct {
	sync.RWMutex
	drivers map[string]DriverFunc
}

func init() {

	internal.drivers = make(map[string]DriverFunc, 0)
	internal.drivers["--"] = func(list ...interface{}) Driver { return &DriverWriter{os.Stdout} }
}

func AttachDriver(name string, f DriverFunc) error {
	internal.Lock()
	defer internal.Unlock()

	if len(name) == 0 {
		return fmt.Errorf("driver name was empty")
	}

	if f == nil {
		return fmt.Errorf("driver function was nil")
	}

	internal.drivers[name] = f
	return nil
}

func DetachDriver(name string) error {
	internal.Lock()
	defer internal.Unlock()

	if len(name) == 0 {
		return fmt.Errorf("driver name was empty")
	}

	if _, exists := internal.drivers[name]; !exists {
		return fmt.Errorf("driver %q not attached", name)
	}
	delete(internal.drivers, name)
	return nil
}

/* parse */
func parse(ctx *RtContextHandle, parameterlist []interface{}) (RtParameters, *Error) {

	/* TODO: add a check for duplicates */

	params := make([]RtParameter, 0)

	if len(parameterlist)%2 != 0 {
		return params, &Error{Code: 101, Severity: 1, Msg: "mismatched parameterlist"}
	}

	var current string
	var skip bool

	for i, param := range parameterlist {
		if i%2 == 0 {
			if str, ok := param.(string); ok {
				if len(str) == 0 {
					skip = true
				}
				current = str

			} else {
				skip = true
			}
		} else {
			if skip {
				skip = false
				continue
			}

			/* parse the current token for the spec -- take into consideration the table of declared tokens */
			spec, err := ParseTokenSpecification(current)
			if err != nil {
				return params, &Error{Code: 101, Severity: 1, Msg: fmt.Sprintf("spec at token %d failed to parse -- %v", i, err)}
			}

			if spec.IsJustName() {
				/* then attempt to read from the declarations */
				if decla, ok := ctx.declarations[spec.Name]; ok {
					spec, err = ParseTokenSpecification(decla)
					if err != nil {
						return params, &Error{Code: 101, Severity: 1, Msg: fmt.Sprintf("spec at token %d failed to parse -- %v", i, err)}
					}
				} else {

					return params, &Error{Code: 101, Severity: 1, Msg: fmt.Sprintf("spec at token %d, no type! \"%s\"", i, current)}
				}
			}

			/* check the value against the spec */
			t := reflect.TypeOf(param)

			if t.String() != spec.GoType() {
				return params, &Error{Code: 101, Severity: 1, Msg: fmt.Sprintf("spec (%s) != type	(%s)", t.String(), spec.GoType())}
			}

			var value interface{}
			value = RtString("__")

			spectype := spec.Type
			if spec.Class == "reference" {
				spectype = "string" /* override */
			}

			switch spectype {
			case "string":
				value = RtString(param.(string))
				break
			case "token":
				value = RtToken(param.(string))
				break
			case "point":
				value = RtPoint(param.([3]float64))
				break
			case "normal":
				value = RtNormal(param.([3]float64))
				break
			case "hpoint":
				value = RtHpoint(param.([4]float64))
				break
			case "interval":
				value = RtInterval(param.([2]float64))
				break
			case "int", "integer":
				value = RtInt(param.(int))
				break
			case "float":
				switch spec.N {
				case 1:
					value = RtFloat(param.(float64))
					break
				case 2:
					if f, ok := param.([2]float64); ok {
						value = [2]RtFloat{RtFloat(f[0]), RtFloat(f[1])}
					}
					break
				case 3:
					if f, ok := param.([3]float64); ok {
						value = [3]RtFloat{RtFloat(f[0]), RtFloat(f[1]), RtFloat(f[2])}
					}
					break
				case 4:
					if f, ok := param.([4]float64); ok {
						value = [4]RtFloat{RtFloat(f[0]), RtFloat(f[1]), RtFloat(f[2]), RtFloat(f[3])}
					}
					break
					/* FIXME */
				}
				break
			case "boolean", "bool":
				value = RtBoolean(param.(bool))
				break
			case "color", "colour":
				value = toRtFloatArray(param.([]float64))
				break
			case "vector":
				value = RtVector(param.([3]float64))
				break
			case "matrix":
				value = RtMatrix(param.([16]float64))
				break
			case "basis":
				/* FIXME */
				break
			case "lighthandle":
				value = RtLightHandle(param.(string))
				break
			case "archivehandle":
				value = RtArchiveHandle(param.(string))
				break
			case "objecthandle":
				value = RtObjectHandle(param.(string))
				break
			default:
				fmt.Printf("unknown type -- %s (%s)\n", t.String(), spec.GoType())

				break
			}

			params = append(params, RtParameter{Token: RtToken(current), Value: value})
			current = ""
		}
	}

	return params, nil
}

/* handle_error */
func handleError(ctx *RtContextHandle, err *Error) {

	if ctx.error_handler == nil {

		panic(err)
	}

	ctx.error_handler(err.Code, err.Severity, err.Msg)
}

/* process */
func process(ctx *RtContextHandle, riber interface{}) *Error {

	if ctx == nil {
		return &Error{Code: 101, Severity: 1, Msg: "invalid context handle"}
	}

	if riber == nil {
		return &Error{Code: 101, Severity: 1, Msg: "invalid rib interface"}
	}

	out := []interface{}{}

	t := reflect.TypeOf(riber)
	if t.Kind() != reflect.Struct {
		return &Error{Code: 101, Severity: 1, Msg: "not a valid struct"}
	}

	name := t.String()

	/* micro context
	 * if within Archive block then store in stack */
	if len(ctx.micro_stack) > 0 {

		mctx := ctx.micro_stack[len(ctx.micro_stack)-1]

		mctx.operations = append(mctx.operations, riber)

		if !mctx.persistent {
			return nil
		}
	}

	/* ==================================== */
	callback := ctx.callback

	for i := 0; i < t.NumField(); i++ {

		field := t.Field(i)
		tag := field.Tag.Get("ri")
		value := reflect.ValueOf(riber).Field(i)

		if tag == "-" { /* don't populate the stream */
			continue
		}

		switch field.Type.Name() {
		case "RtDecoration":

			switch field.Name {
			case "funcname":

				out = append(out, RtFuncName(tag))

				break
			case "params":

				out = append(out, RtAnnotation(tag))

				break
			case "depth":

				d, err := strconv.Atoi(tag)
				if err != nil {
					return &Error{Code: 101, Severity: 1, Msg: err.Error()}
				}

				if d < -1 {
					d = -1
				}

				if d > 1 {
					d = 1
				}

				out = append(out, RtDepth(d))

				break
			default:
				return &Error{Code: 101, Severity: 1, Msg: " unknown decoration in struct"}
			}
			break
		case "RtString":
			v := value.Interface().(RtString)
			if len(string(v)) == 0 {
				return &Error{Code: 101, Severity: 1, Msg: fmt.Sprintf("empty string -- \"%s\"", field.Name)}
			}

			out = append(out, v)

			break
		case "RtToken":
			v := value.Interface().(RtToken)
			if len(string(v)) == 0 {
				return &Error{Code: 101, Severity: 1, Msg: fmt.Sprintf("empty token -- \"%s\"", field.Name)}
			}

			out = append(out, v)
			break
		case "RtParameters":

			params := value.Interface().(RtParameters)

			for j := 0; j < len(params); j++ {

				/* TODO: read the specification of the token and check against the value */
				out = append(out, params[j].Token)
				out = append(out, params[j].Value)
			}

			break
		default:
			out = append(out, value)
			break
		}
	}

	if callback != nil {
		switch name {
		case "ri.RiArchiveRecordComment":
			record := riber.(RiArchiveRecordComment)
			callback(COMMENT, record.Format, record.Params)
			break
		case "ri.RiArchiveRecordVerbatim":
			record := riber.(RiArchiveRecordVerbatim)
			callback(VERBATIM, record.Format, record.Params)
			break
		case "ri.RiArchiveRecordStructure":
			record := riber.(RiArchiveRecordStructure)
			callback(STRUCTURE, record.Format, record.Params)
			break
		}
	}

	if ctx.driver == nil {
		return &Error{Code: 101, Severity: 1, Msg: "need to call Begin first!"}
	}

	indent := func(w io.Writer, depth int) {
		const tabs = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tt\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" /* 60 deep?! */
		if depth <= 0 {
			return
		}

		if depth >= len(tabs) {
			return
		}
		fmt.Fprintf(w, tabs[:depth])
	}

	for _, param := range out {

		t := reflect.TypeOf(param)

		if ctx.within_parameterlist {
			ctx.parameterlist_args++
		}

		switch t.String() {
		case "ri.RtDepth":
			ctx.depth += int(param.(RtDepth))
			break
		case "ri.RtFuncName":
			/* check is the first thing written */
			if ctx.wrote > 0 {
				fmt.Fprintf(ctx.driver, "\n")
			}
			/* TODO, check if we should indent the output */
			indent(ctx.driver, ctx.depth)

			fmt.Fprintf(ctx.driver, "%s", string(param.(RtFuncName)))

			if name != "ri.RiArchiveRecordStructure" {
				fmt.Fprintf(ctx.driver, " ")
			}

			ctx.within_parameterlist = false
			ctx.parameterlist_args = 0
			ctx.wrote++
			break
		case "ri.RtAnnotation":
			switch string(param.(RtAnnotation)) {
			case "parameterlist":
				ctx.within_parameterlist = true
				break
			}
			break
		default:
			ctx.wrote++
			/* TODO: this could be better! */
			if ctx.within_parameterlist { /* part of the parms */
				if ctx.parameterlist_args%2 == 0 { /* value */
					outv := fmt.Sprintf("%v", param)

					if len(outv) > 0 {
						if outv[0] == '[' {
							fmt.Fprintf(ctx.driver, "%s ", outv)
						} else {
							fmt.Fprintf(ctx.driver, "[%v] ", outv)
						}
					}

				} else { /* token */

					fmt.Fprintf(ctx.driver, "%v ", param)

				}
			} else { /* part of the args */

				fmt.Fprintf(ctx.driver, "%v ", param)

			}
			break
		}
	}

	return nil
}
