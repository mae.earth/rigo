RiGO
=================================================

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/mae.earth/rigo)](https://goreportcard.com/report/gitlab.com/mae.earth/rigo)

Implementation of the RenderMan Interface for the Go programming language. This is currently 
based on Pixar's RenderMan Specification version 3.2.1 (November 2005). This implementation 
is still under *active development*, so *expect* holes and bugs. 

The current interface mimics RenderManProServer 21.2

[Online Documentation](https://godoc.org/gitlab.com/mae.earth/rigo)

Install with:

    go get gitlab.com/mae.earth/rigo


A simple example using the built-in RenderMan Interface directly fromt the *ri* package. 

```go
import (
	"gitlab.com/mae.earth/rigo/ri"
)


ri.Begin("out.rib")
	ri.ArchiveRecord(ri.STRUCTURE,"%s RIB-Structure %.1f",ri.RENDERMAN,1.1)
	ri.Display("test.exr","openexr","rgba")
	ri.Format(640,480,1.0)
	ri.Projection("perspective","float fov",30.0)
	ri.Hider("raytrace")
	ri.Integrator("PxrPathTracer","example")
	ri.WorldBegin()
		ri.Translate(0,0,10)
		ri.AttributeBegin()
			ri.Translate(5,5,5)
			light := ri.Light("PxrEnvDayLight","-")		
			ri.Geometry("envsphere","constant float[2] resolution",[2]float64{1024,1024})
		ri.AttributeEnd()
		ri.Illuminate(light,true)
		ri.AttributeBegin()
			ri.Bxdf("PxrDisney","surface","color baseColor",[]float64{0,0,1})
			ri.Sphere(1,-1,1,360)
		ri.AttributeEnd()
	ri.WorldEnd()
ri.End()
```

Output RIB file is thus :.

```rib
##RenderMan RIB-Structure 1.1
Display "test.exr" "openexr" "rgba" 
Format 640 480 1 
Projection "perspective" "float fov" [30] 
Hider "raytrace" 
Integrator "PxrPathTracer" "example" 
WorldBegin 
	Translate 0 0 10 
	AttributeBegin 
		Translate 5 5 5 
		Light "PxrEnvDayLight" "light_bfe579e740226fee" 
		Geometry "envsphere" "constant float[2] resolution" [1024 1024] 
	AttributeEnd 
	Illuminate "light_bfe579e740226fee" 1 
	AttributeBegin 
		Bxdf "PxrDisney" "surface" "color baseColor" [0 0 1] 
		Sphere 1 -1 1 360 
	AttributeEnd 
WorldEnd 
```

In this example we create a context first to work with :-


```go

import (
 
  "gitlab.com/mae.earth/rigo/ri"
  "gitlab.com/mae.earth/rigo"
)

/* create a context to work with */
ctx := rigo.New()

ctx.Begin("unitcube.rib")	
ctx.ArchiveRecord(ri.STRUCTURE, ri.RIB)

ctx.AttributeBegin()
	ctx.Attribute("identifier", "name", "unitcube")
	ctx.Bound([6]float64{-.5, .5, -.5, .5, -.5, .5})
	ctx.TransformBegin()

		points := []float64{.5, .5, .5, -.5, .5, .5, -.5, -.5, .5, .5, -.5, .5}

		ctx.ArchiveRecord(ri.COMMENT,"far face")
		ctx.Polygon(4, ri.P, points)
		ctx.Rotate(90, 0, 1, 0)

		ctx.ArchiveRecord(ri.COMMENT,"right face")
		ctx.Polygon(4, ri.P, points)
		ctx.Rotate(90, 0, 1, 0)

		ctx.ArchiveRecord(ri.COMMENT,"near face")
		ctx.Polygon(4, ri.P, points)
		ctx.Rotate(90, 0, 1, 0)

		ctx.ArchiveRecord(ri.COMMENT,"left face")
		ctx.Polygon(4, ri.P, points)

	ctx.TransformEnd()
	ctx.TransformBegin()

		ctx.ArchiveRecord(ri.COMMENT,"bottom face")
		ctx.Rotate(90, 1, 0, 0)
		ctx.Polygon(4, ri.P, points)

		ctx.TransformEnd()
		ctx.TransformBegin()

		ctx.ArchiveRecord(ri.COMMENT,"top face")
		ctx.Rotate(-90, 1, 0, 0)
		ctx.Polygon(4, ri.P, points)

	ctx.TransformEnd()
ctx.AttributeEnd()
ctx.End()	
```

RIB output of *unitcube.rib* is thus :-

```
##RenderMan RIB
AttributeBegin 
	Attribute "identifier" "name" "unitcube"
	Bound [-.5 .5 -.5 .5 -.5 .5]
	TransformBegin 
		# far face
		Polygon "P" [.5 .5 .5 -.5 .5 .5 -.5 -.5 .5 .5 -.5 .5]
		Rotate 90. 0 1. 0
		# right face
		Polygon "P" [.5 .5 .5 -.5 .5 .5 -.5 -.5 .5 .5 -.5 .5]
		Rotate 90. 0 1. 0
		# near face
		Polygon "P" [.5 .5 .5 -.5 .5 .5 -.5 -.5 .5 .5 -.5 .5]
		Rotate 90. 0 1. 0
		# left face
		Polygon "P" [.5 .5 .5 -.5 .5 .5 -.5 -.5 .5 .5 -.5 .5]
	TransformEnd 
	TransformBegin 
		# bottom face
		Rotate 90. 1. 0 0
		Polygon "P" [.5 .5 .5 -.5 .5 .5 -.5 -.5 .5 .5 -.5 .5]
	TransformEnd 
	TransformBegin 
		# top face
		Rotate -90. 1. 0 0
		Polygon "P" [.5 .5 .5 -.5 .5 .5 -.5 -.5 .5 .5 -.5 .5]
	TransformEnd 
AttributeEnd 
```



